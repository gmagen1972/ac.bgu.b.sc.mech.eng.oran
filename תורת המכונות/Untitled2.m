clc
clear all
% all sizes in [cm]
L=110;
L2=85;
L1=8.411;
L3=L1;
t1=275:0.01:305;    % teta1

% location function
a=-2*L*L1.*sin (t1*pi/180);
b=2*L1*L3.*cos (t1*pi/180)-2*L*L3;
M=2.*L.*L1.*cos (t1*pi/180)-L.^2-L1.^2+L2.^2-L3.^2;

q=a./(a.^2+b.^2).^0.5;
w=b./(a.^2+b.^2).^0.5;
e=(1-M.^2./(a.^2+b.^2)).^0.5;
r=M./(a.^2+b.^2).^0.5;
t4=(atan2 (q,w)-atan2 (r,+e))*180/pi;
% teta4

subplot (3,1,1);
plot (t1,t4);
grid;
xlabel ('teta1[deg]');
ylabel ('teta4[deg]');
title ('graph 1: Location')
gama=acos ((-L1.*cos (t1*pi/180)+L-L3.*cos (t4*pi/180))./L2);

% speed function
omega1=180/1;%[rad/sec]
tdot4=(L1*sin (t1*pi/180-gama)*omega1)./(L3*sin (t4*pi/180+gama));
% radial speed
subplot (3,1,2);
plot (t1,tdot4)
grid;
xlabel ('teta1[deg]');
ylabel ('teta''4[deg/sec]');
title ('graph 2: Speed')

% accelration function
ttag4=(L1*sin (t1*pi/180-gama))./(L3*sin (t1*pi/180+gama));
% d (teta4)/d(teta1)
gamatag=-L1*sin (t1*pi/180+t4*pi/180)./(L2*sin (t4*pi/180+gama));
% d (gama)/d(teta1)
tetadotaeem4=((-L3.*(ttag4).^2.*cos (gama+t4*pi/180)+L2.*gamatag.^2+L1.*cos (gama-t1*pi/180))*omega1).^2./(L3.*sin (gama+t4*pi/180));
% radial accelleration
subplot (3,1,3);
plot (t1,tetadotaeem4);
grid;
xlabel ('teta1[deg]');
ylabel ('tetadotaeem4[deg/sec^2]');
title ('graph 3: Acceleration')