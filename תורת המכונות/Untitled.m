clc
clear all
L=150;%[cm]
H=110;%[cm]
tin=15*pi/180;        % teta in
t1=3*pi/2-tin;        % teta1
costmin=1000;
for L1=1:1:150
    for L2=0:1:110
           L3=L1;
         
           a=-2*L*L1.*sin (t1);
           b=2*L1*L3.*cos (t1)-2*L*L3;
           M=2.*L.*L1.*cos (t1)-L.^2-L1.^2+L2.^2-L3.^2;
           q=a./(a.^2+b.^2).^0.5;
           w=b./(a.^2+b.^2).^0.5;
           e=(1-M.^2./(a.^2+b.^2)).^0.5;
           r=M./(a.^2+b.^2).^0.5;
           t4=atan2 (q,w)-atan2 (r,+e);
           tout=3*pi/2-t4;  %% teta out
           
           cost=tan (tin)-tan (tout)+L/H;
           if abs (costmin)>abs (cost);
               costmin=abs (cost);
               l1=L1;
               l2=L2;
           end
           if costmin==0
               break
           end
    end
end
l1=(L-l2)/2*sin (tin)
l2,cost,costmin