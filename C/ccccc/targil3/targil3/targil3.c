/**********************
* 2 squares program   *
***********************/  

#include <stdio.h>   

void main() {
	int num1,num2,a,b;  	//a,b are the roots of num1,num2 in case they are square
	int temp,d,i;          //varibles for loops

	printf("please enter 2 numbers:\n");  

	scanf("%d%d",&num1,&num2);

	if(num1>0 && num2>0 && num1<=400 && num2<=400)  //checks if num1,num2 are squares between 0-400
	{
		for(a=1;num1!=a*a&&a<=20;a++);  //finds square integer root a (if exists) 
			if(num1==a*a)
			{
				for(b=1;num2!=b*b&b<=20;b++);  //finds square integer root b (if exists)
					if(num2==b*b)
					{
						if(a<b)   //makes a>b if that's not the case
						{
							temp=b;
							b=a;
							a=temp;
						}
						for(i=0;b!=i;i++)   //loop decides how many "mixed" lines will be printed
						{
							for(d=0;b!=d;d++)    //
								printf("b");     //
											     //
							for(d=0;a-b!=d;d++)  //  loop that prints one "mixed" line
									printf("a"); //
												 //
							printf("\n");		 //
						}
						for(i=0;a-b!=i;i++)		//loop decides how many 'a's lines will be printed
						{
							for(d=0;a!=d;d++)	//prints 'a' times 'a' in a line
								printf("a");
							printf("\n");
						}
					}
			}
	}
} //main
