/*=====================================================================
 * Student name: 
 *		Oran Ben Zaken (ID: 039796420)
 *		oranbz@walla.com
 *		Department of Mechanical Engineering
 *	
 *	Course:
 *	   c programming (082)/ Zion Sicsic
 *	   assingment 3	
 *
 *	Date: 5-july-2008
 *	Last submeeting date : 6-july-2008
 *
 *=============================================================================
 * The program contains an electronic sheet, able to be stored with data.
 * The program is able to display the main data table, all of the operations
 * done in it and to edit it.
 *
 *=========================================================================== */


#include <stdio.h>
#include <string.h>

#define ROWS 20
#define COLS 8
#define MAXVAL 30
#define MIN 1
#define MAX 2
#define SUM 3
#define AVERAGE 4

void showMenu();
void showTable(double[ROWS][COLS]);
void showOperations(char operations[MAXVAL][MAXVAL]);
void oneValue(double table[ROWS][COLS],int row,int col);
void scanLocations(double table[ROWS][COLS]);
void enterValues(double table[ROWS][COLS],int beginRow,int beginCol,int endRow,int endCol);
void swapRows(double table[ROWS][COLS],int firstRow,int secondRow);
void sortRows(double table[ROWS][COLS],int col);
int smallerRow(double table[ROWS][COLS],int firstRow,int secondRow,int col);
int getRowFromCell(char temp[]);
int getColFromCell(char temp[]);
double  minv(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol);
double  maxv(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol);
double sum(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol);
double average(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol);
void addOperation(char newOp[], char operations[MAXVAL][MAXVAL], int place);
int numberOfOperations(char operations[MAXVAL][MAXVAL]);
int isValid(char operation[], int rows, int cols);
int getDestRow(char operation[]);
int getDestCol(char operation[]);
int getOperation(char operation[]);
int getBeginRow(char operation[]);
int getBeginCol(char operation[]);
int getEndRow(char operation[]);
int getEndCol(char operation[]);
int strcmp(char op,char); 
void removeOperation(char operations[MAXVAL][MAXVAL], int place);
void update(double table[ROWS][COLS], char operations[MAXVAL][MAXVAL]);
//*********************************************
void main(){

double table[ROWS][COLS]={0};
char operations[MAXVAL][MAXVAL]={0};
int n,x;
int m,i,g,j;
char k,c;
char temp[1000]={0};
char h;
int place=0;
	int DR,BR,ER;		//5.//
	char EC,DC,BC;
	char OP[6]={0};
	char ch1,ch2;


showMenu();

	do{

scanf("%d",&n);
		switch(n){
case 1:
showTable(table);
break;
//-----------------
case 2:
showOperations(operations);
break;
//---------------
case 3:
scanLocations(table);
update(table,operations);
break;
getRowFromCell(temp);
getColFromCell(temp);
//------------------------
case 4:
for(i=0;i<1000;i++)	temp[i]=0;		//initials arr
		
		printf("Enter cell\n");	
			scanf("%s",temp);
		sscanf(temp,"%c%d",&k,&m);
	
		if(k<'A'||k>'H'||m<0||m>19||(temp[2]&&(temp[2]>'9'||temp[2]<'0'))||temp[3]!=0)	{
			printf("incorrect input\n");
			break;
		}		
	oneValue(table,m,k-65);
	update(table,operations);
break;
//-----------------------------------------
case 5:
	printf("Enter operation\n");
for(i=0;i<1000;i++)	temp[i]=0;		//initials arr

scanf("%s",temp);

if(numberOfOperations(operations)==30)	//  
		break;			//when full

if(isValid(temp,30,30))	{	// 1/0
  																	//execute operations:
sscanf(temp,"%c%d%5s%c%d%c%c%d%c",&DC,&DR,OP,&BC,&BR,&ch1,&EC,&ER,&ch2);

	if(getOperation(temp)==1)	table[DR][DC-65]=minv(table,BR,BC-65,ER,EC-65);
	if(getOperation(temp)==2)	table[DR][DC-65]=maxv(table,BR,BC-65,ER,EC-65);
	if(getOperation(temp)==3)	table[DR][DC-65]=sum(table,BR,BC-65,ER,EC-65);
	if(getOperation(temp)==4)	table[DR][DC-65]=average(table,BR,BC-65,ER,EC-65);

	 addOperation(temp,operations,place);		//updating the operation arr
place++;

}//if//
update(table,operations);
printf("Done\n");
break;
//------------------------------------------------------
case 6:
	printf("Enter operation number\n");

	scanf("%d",&x);

	if(x>29||x<0||operations[x][5]==0){			//out of range or empty row
		printf("Done\n");
		break;
	}
removeOperation(operations,x);
place--;
printf("Done\n");
break;
//--------------------------------------------
case 7:
	printf("Enter column\n");
	h=getchar();
	scanf("%c",&c);

	if(c<'A'||c>'H'){		//OUT of range
		printf("incorrect column\n");
			break;
		}
	g=(int)c-65;

	sortRows(table,g);

		for(i=0;i<30;i++)			//initiates operations arrey
		for(j=0;j<30;j++)
			operations[i][j]=0;
	break;
//--------------------------------
case 8:
showMenu();
break;
//---------------------------------
case 9:
	break;
//---------------------------------
default: 
	printf("Incorrect choice\n");

	}//switch//

	}//do//
while(n!=9);

}//main//
//*****************************************************************
//****************************************************************
//**************************************************************

void showMenu(){

printf("Spreadsheet\n");
printf("============\n");
printf("1.Show the table\n");
printf("2.Show the operations\n");
printf("3.Enter values to the table\n");
printf("4.Enter one value to the table\n");
printf("5.Add an operation\n");
printf("6.Remove an operation\n");
printf("7.Sort the table\n");
printf("8.Show the menu\n");
printf("9.Exit\n");
}
//---------------------------------------------

void showTable(double table[ROWS][COLS]){		//1.


char ch;
int i,j;


	for(ch='A';ch<'I';ch++)		//fisrt row
		printf("        %c",ch);
		printf("\n");

		for(i=0;i<79;i++)			//second row
			printf("=");
		printf("\n");

		for (i =0; i < ROWS; i++){		//from 3rd row
		printf("%2d | ",i);
		for (j =0; j < COLS; j++)
			printf("%7.2f |",table[i][j]);
		printf("\n");
		}
		printf("Done\n\n");
		
}
//----------------------------------------------------

void showOperations(char operations[MAXVAL][MAXVAL]){		//2.

	int i;


	for(i=0;i<MAXVAL&&operations[i][0]!=0;i++)
			puts(operations[i]);
printf("\n");	
printf("Done\n\n");
		
}
//---------------------------------------------------------
void oneValue(double table[ROWS][COLS],int row,int col){		//4.

	double value;
	
	printf("Enter value\n");
	scanf("%lf",&value);
 

	table[row][col]=value;
printf("Done\n");
	
}
//----------------------------------------------------------

void scanLocations(double table[ROWS][COLS]){			//3.

	char temp1[1000]={0};
	int BR,ER;	//short names
	char BC,EC;	//
	char ch;	//for ':'
int beginRow,endRow,beginCol,endCol;	//for enterValues func

printf("Enter start cell and end cell\n");
scanf("%s",temp1);
sscanf(temp1,"%c%d%c%c%d",&BC,&BR,&ch,&EC,&ER);

if(BC<'A'||BC>'H'||BR<0||BR>19||ch!=':'||EC<'A'||EC>'H'||ER<0||ER>19||temp1[7]!=0)
	{
	printf("Incorrect input\n");
	return;
	}
beginRow=BR;
endRow=ER;
beginCol=BC-65;
endCol=EC-65;

if(endCol<beginCol||beginRow>endRow)
{
	printf("Incorrect input\n");
	return;
}
enterValues(table,beginRow,beginCol,endRow,endCol);

}//void//
//-------------------------------------------------------

void enterValues(double table[ROWS][COLS],int beginRow,int beginCol,int endRow,int endCol){		//3.

int i,j,k;	//for loops
int nvalues;	//number of values

printf("Enter values\n");
nvalues=(endCol+1-beginCol)*(endRow+1-beginRow);

for(i=beginRow;i<=endRow;i++)
	for(k=0,j=beginCol;j<=endCol&&k<nvalues;j++,k++)
		scanf("%lf",&table[i][j]);
printf("Done\n");

}
//---------------------------------------------------------
void swapRows(double table[ROWS][COLS],int firstRow,int secondRow){		//7.

	int j;
	double tmp;

	for(j=0;j<8;j++){
		tmp=table[firstRow][j];
		table[firstRow][j]=table[secondRow][j];
		table[secondRow][j]=tmp;
	}
}
//----------------------------------
int smallerRow(double table[ROWS][COLS],int firstRow,int secondRow,int col){	//7.

	if(table[firstRow][col]<table[secondRow][col])	return 1;	
	else	return 0;
}
//-----------------------------------
void sortRows(double table[ROWS][COLS],int col){	//7.

int i,j;

for(j=0;j<19;j++)
	for(i=0;i<19;i++){
		if(smallerRow(table,i,i+1,col));	//don't need to swap
		else	swapRows(table,i,i+1);	//need to swap
	}
	printf("Done\n");
}

//-----------------------------------
int getRowFromCell(char cell[]){
	printf("3\n");
	return 1;
}
int getColFromCell(char cell[]){
	printf("4\n");
return 1;
}
//-------------------------------------------------//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                       MIN,MAX..


double  minv(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol)
{
   int  q,w;			
  double min;

  min=table[beginRow][beginCol]; 
for(q=beginRow ;q<=endRow ;q++)
	for(w=beginCol ;w<=endCol ;w++)
		   min=(table[q][w]< min)?table[q][w]:min ;

 return min;
}
//----

double  maxv(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol)
{
   int q,w;			
  double max;

  max=table[beginRow][beginCol]; 
for(q=beginRow ;q<=endRow ;q++)
	for(w=beginCol ;w<=endCol ;w++)
		   max=(table[q][w]> max)?table[q][w]:max ;

 return max;
}
//---------------------------------------------
double sum(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol){

int q,w;
double sum=0;

for(q=beginRow ;q<=endRow ;q++)
	for(w=beginCol ;w<=endCol ;w++)
		sum+=table[q][w];

return sum;
}
//--------------------------------------------
double average(double table[ROWS][COLS], int beginRow, int beginCol, int endRow, int endCol){

	int q,w;
	double average,sum=0;

for(q=beginRow ;q<=endRow ;q++)
	for(w=beginCol ;w<=endCol ;w++)
		sum+=table[q][w];

average=sum/((endCol+1-beginCol)*(endRow+1-beginRow));

return average;
}
//------------------------------------
void addOperation(char newOp[], char operations[MAXVAL][MAXVAL], int place){
	int i;

	for(i=0;i<30;i++)						//copies one operation row to arr
		operations[place][i]=newOp[i];
}
//----------
int numberOfOperations(char operations[MAXVAL][MAXVAL]){
	
int k;

for(k=0;operations[k][5] && k<30; k++);

if(k==30)	printf("Operations table is full\n");
return k;
}
//---------------------------------------------------
int isValid(char operation[], int rows, int cols){

	int i;
	int DR,BR,ER;
	char EC,DC,BC;
	char OP[6]={0};
	char ch1,ch2;


	sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&DC,&DR,OP,&BC,&BR,&ch1,&EC,&ER,&ch2);

	for(i=14; operation[i]!=')'; i++);		//check for extra chars after ')'
	if(operation[i]!=')'){
		printf("incorrect operation\n");
		return 0;
	}

	if(ch1!=':'){
		printf("incorrect operation\n");
		return 0;
	}

	if(getDestCol(operation)==-1||getDestRow(operation)==-1||getOperation(operation)==-1||getBeginCol(operation)==-1)		//if one of the function foun bad input
			return 0;

	if(getBeginRow(operation)==-1||getEndCol(operation)==-1||getEndRow(operation)==-1)
		return 0;


	if(ER<BR||EC<BC){						//
		printf("incorrect operation\n");
		return 0;
	}

	
	return 1;

}//int//
//
//------------------------------------------
//the 7 input functions:

int getDestCol(char operation[]){

char dc;
char g3,g5,g6,g8;		//irrelevant
char g2[6]={0};
int g1,g4,g7;

sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&dc,&g1,g2,&g3,&g4,&g5,&g6,&g7,&g8);

if(dc<'A'||dc>'H'){
	printf("incorrect operation\n");
	return -1;
}
return dc-65;
}
//-----------
int getDestRow(char operation[]){

	int dr;
char g1,g3,g5,g6,g8;		//irrelevant
char g2[6]={0};
int g4,g7;

sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&dr,g2,&g3,&g4,&g5,&g6,&g7,&g8);

if(dr<0||dr>19){
	printf("incorrect operation\n");
	return -1;
}
return dr;
}
//----------
int getOperation(char operation[]){

	char op[6]={0};
int g2,g4,g7;
char g1,g3,g5,g6,g8; 
char st1[]="=min(",st2[]="=max(",st3[]="=sum(",st4[]="=avg(";


	sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&g2,op,&g3,&g4,&g5,&g6,&g7,&g8);

	if(strcmp(op,st1)==0)	return 1;
	if(strcmp(op,st2)==0) return 2;
	if(strcmp(op,st3)==0)	return 3;
	if(strcmp(op,st4)==0) return 4;

	printf("incorrect operation\n");
	return -1;		//wrong input
}
//----------
int getBeginCol(char operation[]){

	char bc;
	char g1,g5,g6,g8;
	char g3[6]={0};
	int g2,g4,g7;

sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&g2,g3,&bc,&g4,&g5,&g6,&g7,&g8);


if(bc<'A'||bc>'H'){
	printf("incorrect operation\n");
	return -1;
}
return bc-65;
}
//-----------------------
int getBeginRow(char operation[]){

	int br;
	int g2,g7;
	char g1,g4,g5,g6,g8;
	char g3[6]={0};

	sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&g2,g3,&g4,&br,&g5,&g6,&g7,&g8);

	if(br<0||br>19){
	printf("incorrect operation\n");
	return -1;
}
return br;
}
//-----------------------
int getEndCol(char operation[]){

	char ec;
	char g1,g4,g6,g8;
	char g3[6]={0};
	int g2,g5,g7;

sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&g2,g3,&g4,&g5,&g6,&ec,&g7,&g8);

if(ec<'A'||ec>'H'){
	printf("incorrect operation\n");
	return -1;
}
return ec-65;
}
//------------------------------------------
int getEndRow(char operation[]){

	int er;
	int g2,g5;
	char g1,g4,g6,g7,g8;
	char g3[6]={0};

sscanf(operation,"%c%d%5s%c%d%c%c%d%c",&g1,&g2,g3,&g4,&g5,&g6,&g7,&er,&g8);

	if(er<0||er>19){
	printf("incorrect operation\n");
	return -1;
}
return er;
}
//------------------------------------------
void removeOperation(char operations[MAXVAL][MAXVAL], int place){
	
	int i,j;

for(j=0;j<30;j++)	operations[place][j]=0;			//deletes the row

for(i=place;i<29;i++){
	strcpy(operations[i],operations[i+1]);		//copies the next row

	for(j=0;j<30;j++)	operations[i+1][j]=0;		//deletes the row that just copied from
}
}
//-----------------------------
void update(double table[ROWS][COLS], char operations[MAXVAL][MAXVAL]){

	int i;

	for(i=0; i<numberOfOperations(operations) ;i++){
	
if(getOperation(operations[i])==1)	//min
table[getDestRow(operations[i])][getDestCol(operations[i])]=minv(table,getBeginRow(operations[i]),getBeginCol(operations[i]),getEndRow(operations[i]),getEndCol(operations[i]));

if(getOperation(operations[i])==2)	//max
table[getDestRow(operations[i])][getDestCol(operations[i])]=maxv(table,getBeginRow(operations[i]),getBeginCol(operations[i]),getEndRow(operations[i]),getEndCol(operations[i]));

if(getOperation(operations[i])==3)	//sum
table[getDestRow(operations[i])][getDestCol(operations[i])]=sum(table,getBeginRow(operations[i]),getBeginCol(operations[i]),getEndRow(operations[i]),getEndCol(operations[i]));

if(getOperation(operations[i])==4)	//avg
table[getDestRow(operations[i])][getDestCol(operations[i])]=average(table,getBeginRow(operations[i]),getBeginCol(operations[i]),getEndRow(operations[i]),getEndCol(operations[i]));
	}//for//
}