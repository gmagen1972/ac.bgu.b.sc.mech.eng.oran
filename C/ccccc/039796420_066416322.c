/*=====================================================================
 * Student name: 
 *		Oran Ben Zaken (ID: 039796420), Guy Avidan (ID 066416322)
 *		oranbz@walla.com, aivdan@bgu.ac.il
 *		Department of Mechanical Engineering
 *	
 *	Course:
 *	   c programming (082)/ Zion Sicsic
 *	   assingment 5	
 *
 *	Date: 27-august-2008
 *	Last submeeting date : 27-august-2008
 *
 *=============================================================================
 * a  small GPS program which finds shortest path between 2 locations 
 *=========================================================================== */




#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct _roads
{
	char dest[20];
	int distance;
	struct _roads *next;
}_roads;


typedef struct _findPath
{
	char name[20];
	char parent[20];
	int distFromOrigin;
	struct _findPath *next;
}_findPath;

typedef struct _cities
{
	char name[20];
	_roads *roads;
	struct _cities *next;
}_cities;

void menu()
{
	printf("GPS\n");
	printf("---\n");
	printf("1. add city\n");
	printf("2. remove city\n");
	printf("3. add road\n");
	printf("4. remove road\n");
	printf("5. Show database\n");
	printf("6. find shortest path\n");
	printf("0. quit\n");
}

_cities* removeRoad(_cities* head,char* citysource,char* cityroad);
void printData(_cities *head);

int cityscan(_cities* head,char* cityname)
{
	while (head->name!=NULL)
	{
		if (!(strcmp(head->name,cityname)))
		{
			return 1;
		}
		head=head->next;
	}
	return 0;
}

int isroad(_cities* head,char* cityname,char* roadname)
{
	_cities *tempC;
	_roads *tempR;
	tempC=head;
	while ((strcmp(tempC->name,cityname))!=0)
	{
		tempC=tempC->next;
	}
	tempR=tempC->roads;
	while ((tempR->dest))
	{
		if ((strcmp(roadname,tempR->dest))!=0)
		{
			tempR=tempR->next;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}
_cities* addCity(_cities* head,char* newcity)
{
	_cities *temp,*p;
	temp=(_cities*)malloc(sizeof(_cities));
	if(!temp)
	{
		printf("memory problem");
		exit(1);
	}

	if (!head||strcmp(head->name,newcity)>=0)
	{
		if(head)
		{	
			if(!(strcmp(head->name,newcity)))
			{
				printf("city already in list\n");
				return head;
			}
		}
		strcpy(temp->name,newcity);
		temp->next=head;
		temp->roads=0;
		return temp;
	}//if

	strcpy(temp->name,newcity);
	temp->roads=0;
	for(p=head;p->next&&strcmp(newcity,p->next->name)>=0;p=p->next);
	if(!(strcmp(p->name,newcity)))
	{
		printf("city already in list\n");
		return head;
	}
	temp->next=p->next;
	p->next=temp;

	return head;
}

_cities * removeCity(_cities* head,char* cityname)
{
	_cities *temp, *tempC;
	temp=head;
	while (temp->name!=0)
	{
		if (!(strcmp(temp->name,cityname)))
		{
			temp=temp->next;
		}
		else if (isroad(temp,temp->name,cityname))
		{
			temp=removeRoad(temp,temp->name,cityname);
			temp=temp->next;
		}
		else
		{
			temp=temp->next;
		}
	}
	if (!(strcmp(head->name,cityname)))
	{
		return head->next;
	}
	tempC=head;
	temp=head->next;
	while (temp->name!=0)
	{
		if (strcmp(temp->name,cityname))
		{
			temp=temp->next;
			tempC=tempC->next;
		}
		else
		{
			tempC->next=temp->next;
			return head;
		}
	}
	tempC->next=0;
	return head;
}

_cities* addRoad(_cities* head,char* citysource,char* cityroad, int dist)
{
	_cities *tempC;
	_roads *tempR, *TempR,*temp=NULL;
	tempC=0;
	tempC=head;
	while (strcmp(tempC->name,citysource)!=0)
	{
		tempC=tempC->next;
	}
	temp=(_roads*)malloc(sizeof(_roads));
	strcpy(temp->dest,cityroad);
	temp->distance=dist;
	temp->next=0;
	if (!(tempC->roads))
	{
		tempC->roads=temp;
		return head;
	}
	if( (strcmp(tempC->roads->dest,cityroad))>0)
	{
		temp->next=tempC->roads;
		tempC->roads=temp;
		return head;
	}
	TempR=tempC->roads;
	tempR=tempC->roads->next;
	while (tempR->dest!=0)
	{
		if ((strcmp(tempR->dest,cityroad))<0)
		{
			tempR=tempR->next;
			TempR=TempR->next;
		}
		else
		{
			TempR->next=temp;
			temp->next=tempR;
			return head;
		}
	}
	TempR->next=temp;
	return head;
}

_cities* removeRoad(_cities* head,char* citysource,char* cityroad)
{
	_cities *tempC;
	_roads *tempR,*TempR;
	tempC=head;
	while (strcmp(tempC->name,citysource)!=0)
	{
		tempC=tempC->next;
	}
	tempR=tempC->roads;
	if (!(strcmp(tempR->dest,cityroad)))
	{
		tempC->roads=tempR->next;
		return head;
	}
	TempR=tempR;
	tempR=tempR->next;
	while (tempR->dest!=0)
	{
		if (strcmp(tempR->dest,cityroad))
		{
			tempR=tempR->next;
			TempR=TempR->next;
		}
		else
		{
			TempR->next=tempR->next;
			return head;
		}
	}
}

void printData(_cities *head)
{
	int n=1;
	_cities *p;
	_roads *temp;

	if(!head)	printf("the list is empty");
	p=head;
	while(p)
	{
		printf("%d. %s\nroads:\n",n,p->name);
		temp=p->roads;
		if(!temp)	printf("no roads\n\n");
		else
			while(temp)
			{
				printf("to %s   distance %d[km]\n",temp->dest,temp->distance);
				temp=temp->next;
			}
		printf("\n");
		p=p->next;
		n++;
	}
	printf("\nthe end of the database\n\n");
}

int scanlist(char *city,_findPath *listB)
{
	while (listB->name)
	{
		if (!(strcmp(city,listB->name)))
		{
			return 1;
		}
		listB=listB->next;
	}
	return 0;
}

_findPath* addkids(_findPath *listA,_roads *head, char *par, int dis)
{
	_findPath *temp;
	temp=(_findPath*)malloc(sizeof(_findPath));
	if (head)
	{
		strcpy(temp->name,head->dest);
		strcpy(temp->parent,par);
		temp->distFromOrigin=head->distance+dis;
		listA->next=temp;
		listA=addkids(listA->next,head->next,par,dis);
		return listA;
	}
	listA->next=0;
	return listA;
}

_findPath* kidsA(_cities *head,_findPath *listA)
{
	_findPath *temp,*Temp;
	_cities *tempC;
	tempC=head;
	Temp=(_findPath*)malloc(sizeof(_findPath));
	while ((strcmp(tempC->name,listA->name))&&(tempC->name))
	{
		tempC=tempC->next;
	}
	temp=listA;
	while (temp->next!=0)
	{
		temp=temp->next;
	}
	if(tempC)
	{
		temp=addkids(temp,tempC->roads,listA->name,listA->distFromOrigin);
	}
	else
	{
		listA=listA->next;
	}
	return listA;
}
_findPath* copytoB(_findPath *listB,_findPath *listA)
{
	_findPath *temp, *Temp,*TempL;
	Temp=(_findPath*)malloc(sizeof(_findPath));
	TempL=listB;
	temp=listB;
	if (!(temp->next))
	{
		Temp->distFromOrigin=listA->distFromOrigin;
		strcpy(Temp->name,listA->name);
		strcpy(Temp->parent,listA->parent);
		Temp->next=0;
		temp->next=Temp;
		return listB;
	}
	temp=listB->next;
	while (temp->next)
	{
		if ((strcmp(temp->name,listA->name))>0)
		{
			Temp->distFromOrigin=listA->distFromOrigin;
			strcpy(Temp->name,listA->name);
			strcpy(Temp->parent,listA->parent);
			Temp->next=0;
			TempL->next=Temp;
			Temp->next=temp;
			return listB;
		}
		temp=temp->next;
		TempL=TempL->next;
	}
	if ((strcmp(temp->name,listA->name))>0)
	{
			Temp->distFromOrigin=listA->distFromOrigin;
			strcpy(Temp->name,listA->name);
			strcpy(Temp->parent,listA->parent);
			Temp->next=0;
			TempL->next=Temp;
			Temp->next=temp;
			return listB;
	}
	Temp->distFromOrigin=listA->distFromOrigin;
	strcpy(Temp->name,listA->name);
	strcpy(Temp->parent,listA->parent);
	Temp->next=0;
	temp->next=Temp;
	return listB;
}
int dis(_findPath *listA,_findPath *listB)
{
	while (strcmp(listA->name,listB->name))
	{
		listB=listB->next;
	}
	if (listA->distFromOrigin>listB->distFromOrigin)
	{
		return 1;
	}
	return 0;
}
_findPath* updataB(_findPath *listB,_findPath *listA)
{
	_findPath *temp;
	temp=listB;
	while (strcmp(temp->name,listA->name))
	{
		temp=temp->next;
	}
	strcpy(temp->parent,listA->parent);
	temp->distFromOrigin=listA->distFromOrigin;
	return listB;
}

void printB(_findPath *listB)
{
	_findPath *list;
	printf("list B\n");
	printf("------\n");
	printf("parent,name,distance\n");

	printf("%s,%s,-\n",listB->parent,listB->name);
	listB=listB->next;
	while (listB->name)
	{
		printf("%s,%s,%d\n",listB->parent,listB->name,listB->distFromOrigin);
		listB=listB->next;
	}
}

int shortroad(_findPath *listB,char *destcity)
{
	while (listB->name)
	{
		if (!(strcmp(listB->name,destcity)))
		{
			return listB->distFromOrigin;
		}
		listB=listB->next;
	}
}

void GPS(_cities *head,char *citysource,char *destcity)
{
	_findPath *listA,*temp;
	_findPath *listB;
	listA=(_findPath*)malloc(sizeof(_findPath));
	listB=(_findPath*)malloc(sizeof(_findPath));
	listA->distFromOrigin=0;
	strcpy(listA->name,citysource);
	strcpy(listA->parent,"-");
	listA->next=0;
	strcpy(listB->name,listA->name);
	strcpy(listB->parent,listA->parent);
	listB->distFromOrigin=listA->distFromOrigin;
	listB->next=0;
	temp=kidsA(head,listA);
	listA=temp;
	listA=listA->next;
	while(listA!=0)
	{
		if (!(scanlist(listA->name,listB)))
		{
			listB=copytoB(listB,listA);
			listA=kidsA(head,listA);
			listA=listA->next;
		}
		else
		{
			if (dis(listA,listB))
			{
				listA=listA->next;
			}
			else
			{
				listB=updataB(listB,listA);
				listA=kidsA(head,listA);
				listA=listA->next;
			}
		}
	}
	printB(listB);
	if (scanlist(destcity,listB))
	{
		printf("the shortest distance from %s to %s is %d\n",citysource,destcity,shortroad(listB,destcity));
	}
	else 
	{
		printf("no connection\n");
	}
}

_cities* function1(_cities *head)
{
	char newcity[20];
	printf("enter city name:");
	gets(newcity);
	gets(newcity);
	head=addCity(head,newcity);
	return head;
}

_cities* function2(_cities *head)
{
	char cityname[20];
	printf("enter city name:");
	gets(cityname);
	gets(cityname);
	head=removeCity(head,cityname);
	return head;
}

_cities* function3(_cities *head)
{
	int dist;
	char roadname[20];
	char cityname[20];
	printf("enter source city:");
	gets(cityname);
	gets(cityname);
	if (!cityscan(head,cityname))
	{
		printf("source city not in list\n");
		return head;
	}
	printf("enter dest city:");
	gets(roadname);
	if (!cityscan(head,roadname))
	{
		printf("dest city not in list\n");
		return head;
	}
	if (!(strcmp(cityname,roadname)))
	{
		printf("sourse and dest are the same\n");
		return head;
	}
	if (isroad(head,cityname,roadname))
	{
		printf("road already exist\n");
		return head;
	}
	printf("enter distance:");
	scanf("%d",&dist);
	head=addRoad(head,cityname,roadname,dist);
	return head;
}

_cities* function4(_cities *head)
{
	int dist;
	char roadname[20];
	char cityname[20];
	printf("enter source city:");
	gets(cityname);
	gets(cityname);
	if (!cityscan(head,cityname))
	{
		printf("source city not in list\n");
		return head;
	}
	printf("enter dest city:");
	gets(roadname);
	if (!cityscan(head,roadname))
	{
		printf("dest city not in list\n");
		return head;
	}
	if (!(isroad(head,cityname,roadname)))
	{
		printf("source and dest are not connected\n");
		return head;
	}
	head=removeRoad(head,cityname,roadname);
	return head;
}

void function6(_cities *head)
{
	char citysource[20];
	char destcity[20];
	printf("enter source city:");
	gets(citysource);
	gets(citysource);
	printf("enter dest city:");
	gets(destcity);
	GPS(head,citysource,destcity);
}

void main ()
{
	int op=1;
	_cities *city;
	_cities *temp;
	_roads *tempr;
	city=(_cities*)malloc(sizeof(_cities));
	temp=(_cities*)malloc(sizeof(_cities));
	tempr=(_roads*)malloc(sizeof(_roads));
	if(!city)
	{
		printf("memory problem");
		exit(1);
	}
	city=0;

	menu();
	scanf("%d",&op);
	while (op!=0)
	{	
		switch (op)
		{
			case 1:
			{
				city=function1(city);
				menu();
				break;
			}
			case 2:
			{
				city=function2(city);
				menu();
				break;
			}
			case 3:
			{
				city=function3(city);
				menu();
				break;
			}
			case 4:
			{
				city=function4(city);
				menu();
				break;
			}
			case 5:
			{
				printData(city);
				menu();
				break;
			}
			case 6:
			{
				function6(city);
				menu();
				break;
			}
			default:
			{
				menu();
				break;
			}

		}
		scanf("%d",&op);
	}
}