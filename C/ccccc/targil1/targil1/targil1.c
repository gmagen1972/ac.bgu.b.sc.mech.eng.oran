/****************
* 3 numbers sum *
****************/

#include <stdio.h>

void main() {

	int num,a,b,c;		

	do{	
		printf("please enter a number between 0 to 100\n");

		scanf("%d",&num);
	}
	while(num<0||num>100);		//ask again for a number in case it's not between 0-100

	if(num>=3)		//numbers under 3 are not good for this calculation
	{
		for(a=0,b=1;a<b&&a<=(num-1);a++)		  //
		{										  //arranges numbers in increasing order, c has to be the largest
			for(b=a+1,c=a+2;b<c&&c>a;b++,c--)     //followed by b
			{									  //
				c=num-(a+b);
				if(a!=b&&b!=c&&c!=a&&a>=0&&b>=0&&c>a)    //makes sure all 3 numbers are not equal, and not negative
				{
					printf("%d,%d,%d\n",a,b,c);			
			}
			}
		}
	}
}