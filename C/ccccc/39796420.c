/*=====================================================================
 * Student name: 
 *		Oran Ben Zaken (ID: 039796420)
 *		oranbz@walla.com
 *		Department of Mechanical Engineering
 *	
 *	Course:
 *	   c programming (082)/ Zion Sicsic
 *	   assingment 4	
 *
 *	Date: 16-august-2008
 *	Last submeeting date : 16-august-2008
 *
 *=============================================================================
 * The program contains recursion programs
 *=========================================================================== */





#include <stdio.h>


void printMenu();
int strlen1(const char str[],int h1);
void strcpy1(char dest[],const char src[],int h1);
void strcat1(char dest[],char const src[],int h1,int h2,int h3);
void sort1(int arr[],int size,int h1,int h2);
int intersect(const int arr1[],const int arr2[],int size1,int size2,int h1,int h2,int h3);
void stringtoarr(const char str[],int arr[],int size);
int FindMid(int arr[],int size);
void cpyWithoutLimits(int arr[],int size,int min,int max);
void cpyWithoutLimits(int arr[],int size,int min,int max);
int min1(int arr[],int size,int h1,int h2);
int max1(int arr[],int size,int h1,int h2);
void strcpy2(int dest[],const int src[],int h1,int lenth);




void main(){

int n;	//for main menu
char s1[21],s2[21];
char ch;
int size1,size2,i;
int st1[21],st2[21];
int str[50],arr[50];
int j,k,m;
int temp[50]={0};
char str2[50];
char str1[50];
char stus[50]={'x'};

do{

printMenu();

	scanf("%d",&n);
	switch(n){

case 1:
	printf("Enter string\n");

	ch=getchar();
	gets(s1);
	printf("Lenth is:%d\n",strlen1(s1,0));
	break;
//-------------------------
case 2:
	printf("Enter string 1:\n");
	ch=getchar();
	gets(s1);
	printf("Enter string 2:\n");

	gets(s2);

	strcat1(s1,s2,0,0,0);

	printf("Result string is:%s\n",s1);
break;
//------------------
case 3:
	printf("Please enter size of array 1:");
	scanf("%d",&size1);
	for(i=0;i<size1;i++){
		printf("arr1[%d]:",i);
		scanf("%d",&st1[i]);
	}
printf("Please enter size of array 2:");
	scanf("%d",&size2);
	for(i=0;i<size2;i++){
		printf("arr2[%d]:",i);
		scanf("%d",&st2[i]);
	}
sort1(st1,size1,0,0);
sort1(st2,size2,0,0);
i=intersect(st1,st2,size1,size2,0,0,0);

	printf("Number of Common Elements:%d\n",i);
break;
//---------------------
case 4:

	printf("Enter string 1:\n");
	ch=getchar();

for(i=0;i<50;i++)	str1[i]='x';
for(i=0;i<50;i++)	str2[i]='x';

gets(str1);
strcpy1(stus,str1,0);
ch='+';
k=0;
while(ch=='+'){

	sscanf(str1,"%d%1c",&temp[k],&ch);
	k++;
	for(i=0;str1[i]!='+'&&str1[i]!=0;i++);
i++;
for(j=0;str1[i]&&str1[i]!='x';i++,j++)	str2[j]=str1[i];
if(ch!='+')	break;
for(i=0;i<50;i++)	str1[i]='x';


	sscanf(str2,"%d%1c",&temp[k],&ch);
	k++;
	for(i=0;str2[i]!='+'&&str2[i]!=0;i++);
i++;
for(j=0;str2[i]&&str2[i]!='x';i++,j++)	str1[j]=str2[i];
if(ch!='+')	break;
for(i=0;i<50;i++)	str2[i]='x';

}//while
temp[k]='\0';

stringtoarr(stus,temp,k);
break;
//----------------------------
case 6:

	printf("Insert array size:");
	scanf("%d",&size1);
	for(i=0;i<size1;i++){
		printf("arr[%d]:",i);
		scanf("%d",&str[i]);
	}
	m=FindMid(str,size1);


	printf("The Median is:%d\n",m);


break;
//------
case 0:
	break;



	

	

	}//switch//
}//do//
while(n!=0);
}//main//









void printMenu(){

	printf("1. Strlen\n");
	printf("2. Strcat\n");
	printf("3. Common elements\n");
	printf("4. Sort Sum\n");
	printf("5. Print all Combination\n");
	printf("6. Find Medial\n");
	printf("7. Find SubString\n");
	printf("8. Hadamard Matrix\n");
	printf("0. Exit\n");
	printf("Choose option number");
}
//***************
int strlen1(const char str[],int h1){

	static int i;
i=h1;
	if(str[i]==0){
		return i;
		i=0;
		}
i++;
strlen1(str,i);
}
//****************
void strcpy1(char dest[],const char src[],int h1){
	static int i;
i=h1;
	dest[i]=src[i];

	if(src[i]==0)	return;
	i++;

	strcpy1(dest,src,i);
}
//***************************
void strcat1(char dest[],char const src[],int h1,int h2,int h3){
static int i,j,a;
a=h1;
i=h2;
j=h3;

if(dest[i]&&a==0){
	i++;
	strcat1(dest,src,a,i,j);	
}

a=1;
if(src[j]){
	dest[i++]=src[j++];
	dest[i]=0;
	strcat1(dest,src,a,i,j);
}

}
//***************************
void sort1(int arr[],int size,int h1,int h2){

	static int k,i;
	int temp;
	k=h1;
	i=h2;

	if(k==size-1||i>=size-1){
		k=0;
		return;
	}
	if(arr[k]>arr[k+1]){
		temp=arr[k];
		arr[k]=arr[k+1];
		arr[k+1]=temp;
	}
	k++;
	if(k==size-1){
		k=0;
		return;
	}
	sort1(arr,size,k,i);

	i++;
	if(i>=size-1)	return;
sort1(arr,size,k,i);

}
//*****************************
int intersect(const int arr1[],const int arr2[],int size1,int size2,int h1,int h2,int h3){

static int i,j,k;
i=h1;
j=h2;
k=h3;

if(j==size2||i>=size1){
	j=0;
	return k;
}

if(arr1[i]==arr1[i+1]){
	i++;
	intersect(arr1,arr2,size1,size2,i,j,k);
}
if(i>=size1)	return k;
if(arr2[j]==arr2[j+1]){
	j++;
	intersect(arr1,arr2,size1,size2,i,j,k);
}
if(i>=size1)	return k;

if(arr1[i]==arr2[j])	k++;
j++;
intersect(arr1,arr2,size1,size2,i,j,k);

i++;
if(i>=size1)	return k;
intersect(arr1,arr2,size1,size2,i,j,k);

return k;

}
//***
void stringtoarr(const char str[],int arr[],int size){
int i;
sort1(arr,size,0,0);

									//				
for(i=0;i<size-1;i++)	printf("%d+",arr[i]);		//
	printf("%d\n",arr[i]);						//


	}
//==================

void strcpy2(int dest[],const int src[],int h1,int lenth){
	static int i;
i=h1;

	if(i==lenth)	return;
	dest[i]=src[i];
	
	i++;
	
	strcpy2(dest,src,i,lenth);
}
//=========================
int max1(int arr[],int size,int h1,int h2){

static int i,max,maxloc;
i=h1;

if(i==size)	return maxloc;
max=arr[h2];
maxloc=h2;
if(arr[i]>max){
	max=arr[i];
	maxloc=i;}
	
i++;
max1(arr,size,i,maxloc);
return maxloc;
}
//=======================
int min1(int arr[],int size,int h1,int h2){

static int i,min,minloc;
i=h1;

if(i==size)	return minloc;
min=arr[h2];
minloc=h2;
if(arr[i]<min){
	min=arr[i];
	minloc=i;}
i++;
min1(arr,size,i,minloc);
return minloc;
}

//=======================
void cpyWithoutLimits(int arr[],int size,int min,int max){
int st[50];
int k,j;

for(k=0,j=0;j<size;j++){
	if(j!=min&&j!=max){
		st[k]=arr[j];
		k++;
	}
}
strcpy2(arr,st,0,size-2);
}
//=================
int FindMid(int arr[],int size){
int a,b,mid;

if(size==1){
	mid=arr[0];
	return mid;
}
if(size==2){
	if(arr[0]<=arr[1]){
		mid=arr[0];
		return mid;
	}
	else{
		mid=arr[1];
		return mid;
	}
}
a=min1(arr,size,0,0);
b=max1(arr,size,0,size-1);

cpyWithoutLimits(arr,size,a,b);
mid=FindMid(arr,size-2);

return mid;


}