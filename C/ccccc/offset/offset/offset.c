#include <stdio.h>

int main() {
int  minutes, hours, offset;
printf("Please enter the current time (hour followed by minutes) and an offset:\n");
scanf("%d%d%d",&hours ,&minutes , &offset);

	minutes = minutes + offset; //Update the number of minutes according to offset.  
	hours = (hours + (minutes/60))%24; //Update the number of hours according to offset.
	minutes = minutes%60; //Update the number of minutes.  
		
	printf("\nhours: %d,  minutes: %d\n",hours,minutes);
 }
