#include <stdio.h>

int main() {

	/************************************************************
	**	We want num1>num2>num3, there are many ways but	**
	**	the required shortest way is as follows:		**
	*************************************************************/
	int num1,num2,num3; //These are the numbers that will be sorted.
	int temp; //This will be a temporary variable that will be used for
		//swapping.

	printf("Please enter three integers:\n");	
	scanf("%d %d %d",&num1 ,&num2 , &num3);

	if(num1 < num2) {	
		//Swap between num1 and num2 so that num1 will hold the 
		//larger value:
		temp=num1;
		num1=num2;
		num2=temp;
	}

	if(num1 < num3) {
		//Swap between num1 and num3 so that num1 will hold the 
		//larger value:
		temp=num1;
		num1=num3;
		num3=temp;
	}

	if(num2 < num3) {
		//Swap between num2 and num3 so that num2 will hold the 
		//larger value:
		temp=num2;
		num2=num3;
		num3=temp;	
	}
	
	printf("The numbers in increasing order are: %d, %d and %d.\n",num3,num2,num1);

}
