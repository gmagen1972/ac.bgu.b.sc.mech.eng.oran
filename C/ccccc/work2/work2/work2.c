/*=====================================================================
 * Student name: 
 *		Oran Ben Zaken (ID: 039796420)
 *		oranbz@walla.com
 *		Department of Mechanical Engineering
 *	
 *	Course:
 *	   c programming (082)/ Zion Sicsic
 *	   assingment1	
 *
 *	Date: 25-june-2008
 *	Last submeeting date : 26-june-2008
 *
 *=============================================================================
 * This program is a version of the 'Game Of Life' theory
 *It runs a grid game between the user to a program
 *The game runs until there is a population determination, or until the number
 of rounds(generations) is done
 *=========================================================================== */



#include <stdio.h>
#include <string.h>


void main(){
	

char grid[70][70]={0};
char temp[1000]={0};						 //temporary arr 
char temp1[1000]={0},temp2[1000]={0};		//temporary arrs for rows, columns
char temp3[1000]={0};						//temporary arr for color input string
int col,row;
int m,k,i,j,x,b;							//m,k,i,j,x,b for loops
int failed=0;							//helps rescaning in cases of incorrect input
int G,R,neighbors;							//'R','G'- colors
char gen,n;								//number of generation; n - number of cells




do			//loop that repeats this block in case of incorrect input


{					//grid size
					//**********
	failed=0;
		
printf("Please enter the width and the height of the grid (10<= height, width<=70):\n");

gets(temp);

m=0;
while(m<1000&&(temp[m]==' '||temp[m]=='	'||(temp[m]<='9'&&temp[m]>='0'))) (m++);//checks that only numbers,'TAB' or 'space' were entered
if(temp[m]!=0) 
{
	printf("Input incorrect! please try again\n");	
	failed=1;
	continue;
 }


	for(m=0,k=0;k<2;m+=2,k++)			//recieving the height and width input
	{

		while(m<1000&&(temp[m]==' '||temp[m]=='	'||temp[m]=='0')) (m++);		//skips the 'space','TAB' and '0' parts before a number 

		
/*(1)*/if((temp[m+2]==0||temp[m+2]==' '||temp[m+2]=='	')&&temp[m+1]!=' '&&temp[m+1]!=0)
		{
				
/*(2)*/			if((temp[m]-48)*10+temp[m+1]-48<=70)			//checks if numbers is inrange
					{
					if(k==0) col=(temp[m]-48)*10+temp[m+1]-48;
					if(k==1) row=(temp[m]-48)*10+temp[m+1]-48;
					}
				else //if(2)	
				 {
					printf("Input incorrect! please try again\n");	
					failed=1;
					break;
				 }
		}//if(1)//
		
		else//if(1)
			{
			printf("Input incorrect! please try again\n");	
			failed=1;
			break;
			}

		
		 

	}//for//

	if(failed==1) continue;
		

	//checking if the input contains more than 2 integers
	while(m<1000&&(temp[m]==' '||temp[m]=='0')) (m++);		//skipping the 'space' parts in arr

	if(temp[m]!=0) 
	{
	printf("Input incorrect! please try again\n");
	failed=1;
	}

}//do//

while(failed==1);

								//number of cells input (n)
								//*************************

do			//loop that repeats this block in case of incorrect input
{
	for(m=0;m<1000;m++) temp[m]=0;	//initials arr

	printf("Please enter the number of living cells(n<%d):\n",col*row);
	
	gets(temp);

	for(m=0;m<1000&&(temp[m]>='0'&&temp[0]<='9')||temp[m]==' '||temp[m]=='	';m++);//checks that only numbers,'TAB' or 'space' were entered

	if(temp[m]!=0)
	{
	printf("Input incorrect! please try again\n");
	continue;
	}

	for(m=0;m<1000&&(temp[m]=='0'||temp[m]==' '||temp[m]=='	');m++);//skips the 'space','TAB' and '0' in the begining arr

	for(k=0;m<1000&&temp[m]!=0&&temp[m]!=' '&&temp[m]!='	';m++,k++);//recieving a number, k- the number of digits in the number

	n=0;
	for(x=1,b=1;b<=k;b++)
	{
		n+=(temp[m-b]-48)*x;		//recievig the number by summering ones with tens, hundreds ,etc..
		x*=10;
	}

	if(temp[m]==0&&n<col*row) break;			//if arr is closed right after one number

	for( ;temp[m]==' '||temp[m]=='	';m++);		//skipping the 'space','TAB' parts after the number
	if(temp[m]!=0)			//checking if there's another number
	{
	printf("Input incorrect! please try again\n");
	continue;
	}
	else			//when only 'space' or TAB after first number
	{
		if (n<col*row) break;
	}

	printf("Input incorrect! please try again\n");
}//do//

while(1);



				
							//row numbers input (x)
							//*********************

do			//loop that repeats this block in case of incorrect input	
{
	failed=0;
	
	for(m=0;m<1000;m++)temp[m]=0;	//initials arr
	
	printf("Please enter the X coordinates (row numbers) of living cells:\n");

	gets(temp);
	
	
	for(m=0;m<1000&&((temp[m]>='0'&&temp[m]<='9')||temp[m]==' '||temp[m]=='	');m++);//checks that only numbers,'TAB' or 'space' were entered

	if(temp[m]!=0)
	{
	printf("Input incorrect! please try again\n");
	failed=1;
	continue;
	}

for(m=0;m<1000&&(temp[m]==' '||temp[m]=='	');m++);//skips the 'space','TAB' at the begining of arr
for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number

	for(b=0,k=0;k<n;k++)		//main loop which recives X values
	{	

			if(temp[m+2]!=0&&temp[m+2]!=' '&&temp[m+2]!='	'&&temp[m+1]!=0&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m]!='0')//for 3 digits number
			{
				printf("22Input incorrect! please try again\n");
				failed=1;
				break;
			}
/*1*/		if(temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0)		//2 digit number
				{
					if((temp[m]-48)*10+temp[m+1]-48<row)
					{	
						temp1[b]=temp[m];			//sending values to row arr
						temp1[b+1]=temp[m+1];
						temp1[b+2]=' ';
						b+=3;
						
						for(m+=2;m<1000&&(temp[m]==' '||temp[m]=='	'||temp[m]=='0');m++);//skips 'space','TAB' after recievs a number
						for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number
						
					}
					else//if number not in range
					{
					printf("33Input incorrect! please try again\n");
					failed=1;
					break;
					}
				}//if(1)//
else				//for one digit number
				{
					temp1[b]=temp[m];
					temp1[b+1]=' ';
					b+=2;
					for(m++;m<1000&&(temp[m]==' '||temp[m]=='	');m++);//skips 'space','TAB' after recievs a number
					for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number				
				}
		
		
		
		if(temp[m]==0)
		{
			if(k+1<n)			//checks if were entered less than n numbers
				{
					printf("44Input incorrect! please try again\n");
					failed=1;
					break;
				}
		}
	}//for(k)//

	if(failed==1) continue;
	

	if(temp[m]!=0)				//checks if there are too many numbers
				{
					printf("55Input incorrect! please try again\n");
					failed=1;
					continue;
				}
}//do//
while(failed==1);






									

do			//loop  asks again for Y values in case of 2 idenical locations on grid
{

				
							//column numbers input (Y)
							//************************

do			//loop that repeats this block in case of incorrect input	
{

for(m=0;m<1000;m++)temp2[m]=0;	//initials arr
for(i=0;i<row;i++)
	for(j=0;j<col;j++)	grid[i][j]=0;


	failed=0;
	
	for(m=0;m<1000;m++)temp[m]=0;	//initials arr
	
	printf("Please enter the Y coordinates (col numbers) of living cells:\n");

	gets(temp);
	
	
	for(m=0;m<1000&&((temp[m]>='0'&&temp[m]<='9')||temp[m]==' '||temp[m]=='	');m++);//checks that only numbers,'TAB' or 'space' were entered

	if(temp[m]!=0)
	{
	printf("Input incorrect! please try again\n");
	failed=1;
	continue;
	}

for(m=0;m<1000&&(temp[m]==' '||temp[m]=='	');m++);//skips the 'space','TAB' at the begining of arr
for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number

	for(b=0,k=0;k<n;k++)		//main loop which recives Y values
	{	

			if(temp[m+2]!=0&&temp[m+2]!=' '&&temp[m+2]!='	'&&temp[m+1]!=0&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m]!='0')//for 3 digits number
			{
				printf("22Input incorrect! please try again\n");
				failed=1;
				break;
			}
/*1*/		if(temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0)		//2 digit number
				{
					if((temp[m]-48)*10+temp[m+1]-48<col)
					{							
						temp2[b]=temp[m];			//sending values to col arr
						temp2[b+1]=temp[m+1];
						temp2[b+2]=' ';
						b+=3;
						for(m+=2;m<1000&&(temp[m]==' '||temp[m]=='	');m++);//skips 'space','TAB' after recievs a number
						for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number
					}
					
					else//if number not in range
					{
					printf("33Input incorrect! please try again\n");
					failed=1;
					break;
					}
				}//if(1)//
else				//for one digit number
				{
					temp2[b]=temp[m];
					temp2[b+1]=' ';
					b+=2;
					for(m++;m<1000&&(temp[m]==' '||temp[m]=='	');m++);//skips 'space','TAB' after recievs a number
					for( ;temp[m]=='0'&&temp[m+1]!=' '&&temp[m+1]!='	'&&temp[m+1]!=0;m++);//skips '0' before number				
}
		
		
		
		if(temp[m]==0)
		{
			if(k+1<n)			//checks if were entered less than n numbers
				{
					printf("44Input incorrect! please try again\n");
					failed=1;
					break;
				}
		}

	}//for(k)//

	if(failed==1) continue;
	

	if(temp[m]!=0)				//checks if there are too many numbers
				{
					printf("55Input incorrect! please try again\n");
					failed=1;
					continue;
				}
}//second 'do'//
while(failed==1);



											//storing locations in grid
											//**************************


/*1*/	for(k=0,m=0,b=0;k<n;k++)		//loop runs for each spot on grid
	{
/*2*/		for( ;temp1[m]!=0; )			//pulls out X values
			{
				if(temp1[m+1]==' ')		//one digit
				{
					i=temp1[m]-48;		//go to next number
					m+=2;
					break;
				}
				else					//2 digits
				{
					i=(temp1[m]-48)*10+temp1[m+1]-48;
					m+=3;			//go to next number
					break;
				}
			}//for(2)//



				
/*3*/			for( ;temp2[b]!=0;m++)	//pulls out Y values
			{
				if(temp2[b+1]==' ')		//one digit
				{
					j=temp2[b]-48;		//go to next number
					b+=2;
					break;
				}
				else					//2 digits
				{
					j=(temp2[b]-48)*10+temp2[b+1]-48;
					b+=3;		//go to next number
					break;
				}
			}//for(3)//

	if(grid[i][j]==0)		//stores locations in grid
		grid[i][j]='G';

	else{
		printf("321Input incorrect! please try again\n");	//if there are 2 idenical locations
		failed=1;
		break;
		}

}//for(1)//




}//first 'do'//
while(failed==1);






											//color input
											//***********

do			//loop that repeats this block in case of incorrect input
{
failed=0;

	printf("Please enter the colors (R or G) of living cells:\n");
	gets(temp3);

	for(m=0;temp3[m]<1000&&(temp3[m]=='R'||temp3[m]=='G'||temp3[m]==' '||temp3[m]=='	');m++);//checks that only G,R 'space' or TAB  were entered
	if(temp3[m]!=0)
	{
		printf("Input incorrect! please try again\n");	
		failed=1;
		continue;
		}

	for(m=0;m<1000&&(temp3[m]==' '||temp3[m]=='	');m++);		//skips 'spcae' and 'TAB' at begining of string 
k=0;


	for(i=0;i<row;i++)		//loop skips rows when scaning grid
	{
		for(j=0;j<col;j++)	//loop runs on each row
		{
	/*1*/		if(grid[i][j]!=0)
			{
				if(temp3[m]=='R')
				{
					grid[i][j]='R';	//when it's necesery to chang a location to 'R' 
					for(m++;m<1000&&(temp3[m]==' '||temp3[m]=='	');m++);		//skips 'spcae' and 'TAB' between letters
				}
				else		//for G
					if(temp3[m]=='G')
					{
						grid[i][j]='G';
						for(m++;m<1000&&(temp3[m]==' '||temp3[m]=='	');m++);		//skips 'space' and 'TAB' between letters
					}
					else	//not enough nubers were entered
						{
						printf("Input incorrect! please try again\n");	
						failed=1;
						break;
						}
			}//if(1)
		}//for(j)//
if(failed==1)	break;

	}//FOR(i)//
if(failed==1)	continue;

if(temp3[m]!=0)	//too many letters
		{
		printf("Input incorrect! please try again\n");	
		failed=1;
		continue;
		}


}//do//

while(failed==1);






											//initial grid output
											//*******************
printf("\n");

for(i=0;i<row;i++)	//loop takes to next row after one printed
{
	for(j=0;j<col;j++)	//loop that prints one row
	{
		if(!grid[i][j]) printf("-");
		else{
			if(grid[i][j]=='R') printf("R");
			else printf("G");
			}
	}//for(j)//
	printf("\n");
}//for(i)//





								//number of GENERATIONS input (gen)
								//*************************

do			//loop that repeats this block in case of incorrect input
{
	for(m=0;m<1000;m++) temp[m]=0;	//initials arr

	printf("Please enter the number of genertions(>0):\n");
	
	gets(temp);

	for(m=0;m<1000&&(temp[m]>='0'&&temp[0]<='9')||temp[m]==' '||temp[m]=='	';m++);//checks that only numbers,'TAB' or 'space' were entered

	if(temp[m]!=0)
	{
	printf("Input incorrect! please try again\n");
	continue;
	}

	for(m=0;m<1000&&(temp[m]=='0'||temp[m]==' '||temp[m]=='	');m++);//skips the 'space','TAB' and '0' in the begining arr

	for(k=0;m<1000&&temp[m]!=0&&temp[m]!=' '&&temp[m]!='	';m++,k++);//recieving a number, k- the number of digits in the number

	gen=0;
	for(x=1,b=1;b<=k;b++)
	{
		gen+=(temp[m-b]-48)*x;		//recievig the number by summering ones with tens, hundreds ,etc..
		x*=10;
	}

	if(temp[m]==0) break;			//if arr is closed right after one number

	for( ;temp[m]==' '||temp[m]=='	';m++);		//skipping the 'space','TAB' parts after the number
	if(temp[m]!=0)			//checking if there's another number
	{
	printf("Input incorrect! please try again\n");
	continue;
	}
	else			//when only 'space' or TAB after first number
		 break;

	printf("Input incorrect! please try again\n");
}//do//

while(1);





//first gen calculating



					//CALCULATING A GENERATION
					//*************************



/*1*/	for(i=0;i<row;i++)			//the scaning of cell's neughbors is from up to down, then right to left
		{
/*2*/		for(j=0;j<col;j++)
			{
			R=0;			//the types of a possible neighbor
			G=0;			//
			neighbors=0;
m=0;
k=0;
/*3*/ 			for(m=i-1,k=j-1;k<=j+1||k==col-1;k++)		//loop skips columns
				{
					if(k==col) k=0;
					if(k==-1) k=col-1;
	
/*4*/				for( ;m<=i+1;m++)			//loop goes down in rows
					{
						if(m==i&&k==j) continue;	//center cell
						
						if(m==-1)
						{
							if(grid[row-1][k]=='R')	R++;
							if(grid[row-1][k]=='G')	G++;
						neighbors++;
						if(neighbors==8) break;
						m=0;
						}
						
						if(m==i&&k==j) continue;	//center cell

						if(m==row)					//goes down
						{
						
						if(grid[0][k]=='G') G++;
						if(grid[0][k]=='R') R++;
						neighbors++;

						if(neighbors==8) break;

						m=i-1;
						break;
						}//if(m)//

					//a regular cell scan
			if(grid[m][k]=='G') G++;
			if(grid[m][k]=='R') R++;
			neighbors++;

			if(neighbors==8) break;

			if(m==i+1)	//takes back to first row
			{
				m=i-1;
				break;
			}

				}//for4//

				if(neighbors==8) break;	//when all neighbors sacnned

	}//for3//


if(grid[i][j]!=0)
		{
			if(G+R<2) grid[i][j]=0;
			if(G+R>3) grid[i][j]=0;
		}
		else
		{
			if(G+R==3)
			{
				if(R>G) grid[i][j]='R';
				if(G>R) grid[i][j]='G';
			}
		}
			


	


			}//for2//
}//for1//




//first gen printing


											//grid output
											//***********
printf("\n");

for(i=0;i<row;i++)	//loop takes to next row after one printed
{
	for(j=0;j<col;j++)	//loop that prints one row
	{
		if(!grid[i][j]) printf("-");
		else{
			if(grid[i][j]=='R') printf("R");
			else printf("G");
			}
	}//for(j)//
	printf("\n");
}//for(i)//
j--;




///grid scan:
//************


for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
if(R==0&&G!=0)	gen=0;

if(G==0&&R!=0)	gen=0;

if(R==0&&G==0)	gen=0;




//************************************************************
	



	
if(gen>0)
{
	do//long do
	{
//red input



do			//loop that repeats this block in case of incorrect input


{					// player input
					//*************
	failed=0;
	for(m=0;m<1000;m++) temp[m]=0;	//initials arr

printf("Red player, please enter the coordinates of the cell:\n");

gets(temp);
i=0;
j=0;
m=0;
while(m<1000&&(temp[m]==' '||temp[m]=='	'||(temp[m]<='9'&&temp[m]>='0'))) (m++);//checks that only numbers,'TAB' or 'space' were entered
if(temp[m]!=0) 
{
	printf("Input incorrect! please try again\n");	
	failed=1;
	continue;
 }


	for(m=0,k=0;k<2;k++)			//recieving the	x and y input
	{

		while(m<1000&&(temp[m]==' '||temp[m]=='	'||temp[m]=='0')) (m++);		//skips the 'space','TAB' and '0' parts before a number 

		
/*(1)*/if((temp[m+2]==0||temp[m+2]==' '||temp[m+2]=='	')&&temp[m+1]!=' '&&temp[m+1]!=0)// 2 digits
		{
				
/*(2)*/			if((temp[m]-48)*10+temp[m+1]-48<row)//in range
					{
					if(k==0) i=(temp[m]-48)*10+temp[m+1]-48;
					if(k==1) j=(temp[m]-48)*10+temp[m+1]-48;
					m+=2;
					}
				else //if(2)	//out of range
				 {
					printf("11Input incorrect! please try again\n");	
					failed=1;
					break;
				 }
		}//if(1)//
		
		else//if(1)
		{
			if(temp[m+1]==' '||temp[m+1]=='	'||temp[m+1]==0)  i=temp[m];	//one digit
			m++;
		}
		
		 

	}//for//

	if(failed==1) continue;
		

	//checking if the input contains more than 2 integers
	while(m<1000&&(temp[m]==' '||temp[m]=='0'||temp[m]=='	')) (m++);		//skipping the 'space', TAB parts 

	if(temp[m]!=0) 
	{
	printf("22Input incorrect! please try again\n");
	failed=1;
	}

}//do//

while(failed==1);

grid[i][j]='R';






	

					//CALCULATING A GENERATION
					//*************************



/*1*/	for(i=0;i<row;i++)			//the scaning of cell's neughbors is from up to down, then right to left
		{
/*2*/		for(j=0;j<col;j++)
			{
			R=0;			//the types of a possible neighbor
			G=0;			//
			neighbors=0;
m=0;
k=0;
/*3*/ 			for(m=i-1,k=j-1;1;k++)		//loop skips columns
				{
					if(k==col) k=0;
					if(k==-1) k=col-1;
	
/*4*/				for( ;m<=i+1;m++)			//loop goes down in rows
					{
						if(m==i&&k==j) continue;	//center cell
						
						if(m==-1)
						{
							if(grid[row-1][k]=='R')	R++;
							if(grid[row-1][k]=='G')	G++;
						neighbors++;
						if(neighbors==8) break;
						m=0;
						}
						
						if(m==i&&k==j) continue;	//center cell

						if(m==row)					//goes down
						{
						
						if(grid[0][k]=='G') G++;
						if(grid[0][k]=='R') R++;
						neighbors++;

						if(neighbors==8) break;

						m=i-1;
						break;
						}//if(m)//

					//a regular cell scan
			if(grid[m][k]=='G') G++;
			if(grid[m][k]=='R') R++;
			neighbors++;

			if(neighbors==8) break;

			if(m==i+1)	//takes back to first row
			{
				m=i-1;
				break;
			}

				}//for4//

				if(neighbors==8) break;	//when all neighbors sacnned

			}//for3//

			


		if(grid[i][j]!=0)
		{
			if(G+R<2) grid[i][j]=0;
			if(G+R>3) grid[i][j]=0;
		}
		else
		{
			if(G+R==3)
			{
				if(R>G) grid[i][j]='R';
				if(G>R) grid[i][j]='G';
			}
		}


			}//for2//
}//for1//




//first gen printing


											//grid output
											//***********
printf("\n");

for(i=0;i<row;i++)	//loop takes to next row after one printed
{
	for(j=0;j<col;j++)	//loop that prints one row
	{
		if(!grid[i][j]) printf("-");
		else{
			if(grid[i][j]=='R') printf("R");
			else printf("G");
			}
	}//for(j)//
	printf("\n");
}//for(i)//

j--;



///grid scan:
//************
failed=0;

for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
if(R==0&&G!=0)
{
	printf("The game is over, the winner is the green player!\n");
	break;
}
if(G==0&&R!=0)
{
	printf("The game is over, the winner is the red player!\n");
	break;
}
if(R==0&&G==0)
{
	printf("The game is over, there is no winner\n");
	break;
}








if(gen>0)
{


//program calculates a move
//**************************

	
	for(b=0,i=0;i<row;i++)		//seach for first red with 2-3 neighbors
	{
		for(j=0;j<col;j++)
		{
			if(grid[i][j]=='R')
			{
				
				if(R+G==2||R+G==3)	//found
				{
					grid[i][j]='G';
					b=1;
					break;
				}
			}//if//

		
		}//for(j)//
	if(b==1) break;
	}//for(i)//

	if(b==0)			//picks the first red cell
	{
		for(i=0;i<row;i++)
		{
			for(j=0;j<col;j++)
			{
				if(grid[i][j]=='R')
				{
				grid[i][j]='G';
				b=1;
				break;
				}
			}//for(j)
		if(b==1)	break;
		}//for(i)

	}//if(b)//





	
	
	
	//prints the MOVE

	printf("The grren player has choosen cell: (%d,%d)\n",i,j);

//calculating next generation

	

					//CALCULATING A GENERATION
					//*************************



/*1*/	for(i=0;i<row;i++)			//the scaning of cell's neughbors is from up to down, then right to left
		{
/*2*/		for(j=0;j<col;j++)
			{
			R=0;			//the types of a possible neighbor
			G=0;			//
			neighbors=0;
m=0;
k=0;
/*3*/ 			for(m=i-1,k=j-1;1;k++)		//loop skips columns
				{
					if(k==col) k=0;
					if(k==-1) k=col-1;
	
/*4*/				for( ;m<=i+1;m++)			//loop goes down in rows
					{
						if(m==i&&k==j) continue;	//center cell
						
						if(m==-1)
						{
							if(grid[row-1][k]=='R')	R++;
							if(grid[row-1][k]=='G')	G++;
						neighbors++;
						if(neighbors==8) break;
						m=0;
						}
						
						if(m==i&&k==j) continue;	//center cell

						if(m==row)					//goes down
						{
						
						if(grid[0][k]=='G') G++;
						if(grid[0][k]=='R') R++;
						neighbors++;

						if(neighbors==8) break;

						m=i-1;
						break;
						}//if(m)//

					//a regular cell scan
			if(grid[m][k]=='G') G++;
			if(grid[m][k]=='R') R++;
			neighbors++;

			if(neighbors==8) break;

			if(m==i+1)	//takes back to first row
			{
				m=i-1;
				break;
			}

				}//for4//

				if(neighbors==8) break;	//when all neighbors sacnned

			}//for3//

			


		if(grid[i][j]!=0)
		{
			if(G+R<2) grid[i][j]=0;
			if(G+R>3) grid[i][j]=0;
		}
		else
		{
			if(G+R==3)
			{
				if(R>G) grid[i][j]='R';
				if(G>R) grid[i][j]='G';
			}
		}


			}//for2//
}//for1//





//prints the grid
	

											//grid output
											//***********
printf("\n");

for(i=0;i<row;i++)	//loop takes to next row after one printed
{
	for(j=0;j<col;j++)	//loop that prints one row
	{
		if(!grid[i][j]) printf("-");
		else{
			if(grid[i][j]=='R') printf("R");
			else printf("G");
			}
	}//for(j)//
	printf("\n");
}//for(i)//

gen--;



///grid scan:
//************


for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
if(R==0&&G!=0)
{
	printf("The game is over, the winner is the green player!\n");	
	break;
}
if(G==0&&R!=0)
{
	printf("The game is over, the winner is the red player!\n");	
	break;
}
if(R==0&&G==0)
{
	printf("The game is over, there is no winner\n");	
	break;
}









if(gen==0)
{
		
///grid scan:   //determens who won
//************

for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
	
	if(R>G)
{
	printf("The game is over, the winner is the red player!\n");
	break;
}
if(G>R)
{
	printf("The game is over, the winner is the green player!\n");
	break;
}
if(R==G)
{
	printf("The game is over, there is no winner\n");
	break;
}
}//if(gen==0)//
}
else	//if(gen>0)//
{
	
		
///grid scan:   //determens who won
//************

for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
	
	if(R>G)
{
	printf("The game is over, the winner is the red player!\n");
	break;
}
if(G>R)
{
	printf("The game is over, the winner is the green player!\n");
	break;
}
if(R==G)
{
	printf("The game is over, there is no winner\n");
	break;
}


}//else//
}
while(gen>0);

}//if(gen>0)-the first//

else	//gen==0//
{		
///grid scan:   //determens who won
//************

for(G=0,R=0,i=0;i<row;i++)
{
	for(j=0;j<col;j++)
	{
		if(grid[i][j]=='R') R++;
		if(grid[i][j]=='G') G++;
	}
}
	
	if(R>G)
{
	printf("The game is over, the winner is the red player!\n");
	
}
if(G>R)
{
	printf("The game is over, the winner is the green player!\n");
	
}
if(R==G)
{
	printf("The game is over, there is no winner\n");
	
}

}//else//



}//main//
