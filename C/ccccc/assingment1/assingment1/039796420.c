/*=====================================================================
 * Student name: 
 *		Oran Ben Zaken (ID: 039796420)
 *		oranbz@walla.com
 *		Department of Mechanical Engineering
 *	
 *	Course:
 *	   c programming (082)/ Zion Sicsic
 *	   assingment1	
 *
 *	Date: 9-june-2008
 *	Last submeeting date : 12-june-2008
 *
 *======================================================================
 * this program contains 3 different programs, presented in a main menu
 * each sub-program is individual
 *====================================================================== */



#include <stdio.h>

void main()	{


	int n;					//variable for main menu

	int num,x,y,z;			//variables for 'Three Number Sum'

	int k,d;				//variables for 'Numbers According To a Digit'

	int num1,num2,a,b;		//variables for 'Two Squares'
	int temp,j,i;			//variables for 'Two Squares'



	do		//primary loop which displays main menu once done with one of the sub-programs
	{



	do					//main menu is introduced, in which user can choos a program to run:
	{
		printf("please choose one of the options below:\n1 - three numbers sum\n2- numbers according to a digit\n3 - two squares\n4 - quit\n");
		scanf("%d",&n);
	}
	while(n<0||n>4);			//in case user didn't insert number between 1-4

	if(n==1)
	
	{
			/********************
			* Three Numbers Sum *
			********************/
	
		//'num' is the number between 0-100 user inserts
	//x,y,z are three natural numbres that sum 'num'


	do
	{	
		printf("please enter a number between 0 to 100\n");

		scanf("%d",&num);
	}

	while(num<0||num>100);		//ask again for a number in case it's not between 0-100

	if(num>=3)		//numbers under 3 are not good for this calculation
	{
		for(x=0,y=1;x<y&&x<=(num-1);x++)		  //
		{										  //arranges numbers in increasing order, z has to be the largest
			for(y=x+1,z=x+2;y<z&&z>x;y++,z--)     //followed by y
			{									  //
				z=num-(x+y);
				if(x!=y&&y!=z&&z!=x&&x>=0&&y>=0&&z>x)    //makes sure all 3 numbers are not equal, and not negative
				{
					printf("%d,%d,%d\n",x,y,z);			
				}
			}//for (y)//

		}//for (x)//

	}//if//
	//end of program

}//if(n==1)//



	
	if(n==2)
					/******************************
					* Numbers Accordind To a Digit*
					******************************/
	{
	
	//'k' is the integer fo numbers in series between 1-100
	//'d' is the number user entered
	
	do{
		printf("please enter a number between 0 to 9:\n");
		scanf("%d",&d);
		}
	while(d>9||d<0);          //makes sure user inserts number between 0-9

	if(d==0)				  //privat case when X should be printed only at products of 10
	{
		for(k=1;k<100;k++)
		{
			if(k>=10&&k%10==0)
				printf("X ");
			else
				printf("%d ",k);
		}
		printf("X\n");
	}

	else				//for all other numbers
	{
		for(k=1;k<100;k++)					//loop prints all 'k's increasingly	unless spesific 'k' answers one
		{									// of the 3 conditions:
			if(((k-d)>=0&&(k-d)%10==0)||		//d in ones
				(k/10==d)||						//d in tens
				(k>=10&&k-(k/10)*10+k/10==d))	//d is the sum of tens and ones digits
			
			printf("X ");
			
			else
				printf("%d ",k);
			
		}//for//

		if(d==1)				//refers to last number - 100
			printf("X ");		//		
		else					//
			printf("100\n");	//
		
	}//else//
	//end of program
	
	}//if(n==2)//
	




	if(n==3)
	{
				/************************
				* Two Squares Program   *
				*************************/  
  
		//'num1', 'num2' are numbers user inserts
	 	//'a', 'b' are the roots of 'num1' ,'num2' in case they are square
        //'j', 'i' are variables for loops
		//'temp' - helps switch 'a' with 'b' if needed


	printf("please enter 2 numbers:\n");  

	scanf("%d%d",&num1,&num2);

	if(num1>0 && num2>0 && num1<=400 && num2<=400)  //checks if num1,num2 are squares between 0-400
	{
		for(a=1;num1!=a*a&&a<=20;a++);  //finds square integer root a (if exists) 
			if(num1==a*a)
			{
				for(b=1;num2!=b*b&&b<=20;b++);  //finds square integer root b (if exists)
					if(num2==b*b)
					{
						if(a<b)   //makes a>b if that's not the case
						{
							temp=b;
							b=a;
							a=temp;
						}
						for(i=0;b!=i;i++)   //loop decides how many "mixed" lines will be printed
						{
							for(j=0;b!=j;j++)    //
								printf("b");     //
											     //
							for(j=0;a-b!=j;j++)  //  loop that prints one "mixed" line
									printf("a"); //
												 //
							printf("\n");		 //
						}//for (i)
						for(i=0;a-b!=i;i++)		//loop decides how many 'a's lines will be printed
						{
							for(j=0;a!=j;j++)	//prints 'a' times 'a' in a line
								printf("a");
							printf("\n");
						}//for (i)//
					}//if (b)//
			}//if (a)//
	}//for(a)//

} //end of program

}//if(n==3)



while(n>0&&n<4);  //end of primary loop which takes it back to main menu

}//main

