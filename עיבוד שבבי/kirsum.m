function milling

T=0.0133;%cycle time
t=[0:5*exp(-5)*1e-3:T];%time in[sec]
%  (fi=wt=150*pi*t)
fi=150*pi*t;%in [rad]
fi_deg=fi*180/pi;%in [deg]

%delta_y  -  ������ �,�
%the deflection in the y direction
%-------------------------------------------------------------

%function of fi:
delta_y=(-3.36*cos(2*fi-0.271)-1.01*sin(2*fi-0.271)+1.05);
subplot(3,1,1)
plot(fi_deg,delta_y), grid on,title('The deflection in the Y direction as a function of spinning angle')
xlabel('\phi [deg]'),ylabel('\delta y [mm]')
%function of time:
%  (fi=wt=450*pi*t)
delta_yy=(-3.36*cos(300*pi*t-0.271)-1.01*sin(300*pi*t-0.271)+1.05);
subplot(3,1,2)
plot(t,delta_yy), grid on,title('The deflection in the Y direction as a function of time')
xlabel('t [sec]'),ylabel('\delta y [mm]')

%Fy - ���� � 
%Forces in the y direction
%----------------------------------------------

%function of fi: 
fi1=[0:0.01:pi/2];
fi2=[2*pi/3:0.01:7*pi/6];
fi3=[4*pi/3:0.01:11*pi/6];
Fy=2100*(1-cos(2*fi1)-0.3*sin(2*fi1));

 subplot(3,1,3)
plot(180/pi*fi1,Fy,'b',180/pi*fi2,Fy,'b',180/pi*fi3,Fy,'b')
grid on
xlabel('\phi [deg]'), ylabel('Fy [N]'), title('The force in the Y direction as a function of spinning angle')



%maximum deflection in the Y direction -  ���� �
display(['The max deflection in the Y direction is: ',num2str(max(delta_y)),'mm'])

end