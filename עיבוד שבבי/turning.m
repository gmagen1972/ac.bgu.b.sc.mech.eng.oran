function turning

clc
d1=16.5;
d2=12;
Ks=1700;
L=input('Enter length of rod L in [mm]: ');
a=input('Enter depth of cut (a) in [mm]: ');
v=input('Enter cutting speed (v) in [mm]: ');
f0=input('Enter feed rate (f0) [mm/rev]: ');
disp('----------------------------------------------------------------------------------')
l=[0:0.01:L];

%**************************************************************************
%���� �

subplot(2,1,1) %f0 changes

% [20%]
y=2.411e-10*(a*Ks*(0.2*f0))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'b');   
hold on

% [52.5%]
y=2.411e-10*(a*Ks*(0.525*f0))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'g');   
hold on

% [85%]
y=2.411e-10*(a*Ks*(0.85*f0))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'r');   
hold on

% [117.5%]
y=2.411e-10*(a*Ks*(1.175*f0))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'k');   
hold on

% [150%]
y=2.411e-10*(a*Ks*(1.5*f0))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'c');   


 title('Change in diameter as a function of tool location l  [fo changes]');
xlabel('l[mm]');ylabel('delta d [mm]');grid on
legend('20%fo','52.5%fo','85%fo','117.5%fo','150%fo')



subplot(2,1,2) %a changes

% [20%]
y=2.411e-10*(f0*Ks*(0.2*a))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'b');   
hold on

% [52.5%]
y=2.411e-10*(f0*Ks*(0.525*a))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'g');   
hold on

% [85%]
y=2.411e-10*(f0*Ks*(0.85*a))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'r');   
hold on

% [117.5%]
y=2.411e-10*(f0*Ks*(1.175*a))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'k');   
hold on

% [150%]
y=2.411e-10*(f0*Ks*(1.5*a))*l.^3;
delta_d=abs(d1-(d2+2*y));
plot(l,delta_d,'c');   
hold on

 title('Change in diameter as a function of tool location l  [a changes]');
xlabel('l[mm]');ylabel('delta d [mm]');grid on
legend('20%a','52.5%a','85%a','117.5%a','150%a')

%**************************************************************************
y=2.411e-10*(f0*Ks*a)*l.^3;
%���� �
% y=2.411e-10*(f0*Ks*a)*l.^3        
% => l=[y/2.411e-10(a*K*s*f0)]^(1/3)        -invert equation
x_max_error=(max(y)/(2.411e-10*(a*Ks*f0)))^(1/3);
display(['The X cordinate of max error is:',num2str(x_max_error)])
%d_final=d2+2y
d_max=d2+2*max(y)
disp('----------------------------------------------------------------------------------')
%**************************************************************************
%���� �
disp('Power Calculations')
disp('****************************')
v_user=input('Please enter cutting speed v in [m/min]: ');
a_user=input('Please enter depth of cut in [mm]: ');
f0_user=input('Please enter feed in [mm/rev]: ');
disp('=>')

p_given=10.5;
eff=0.7;%efficiency

p_tested=((v_user*Ks*a_user*f0_user)/eff)/60000;
display(['The calculated power is:',num2str(p_tested),'[kW]'])
if p_tested<p_given
    disp('The calculated power is in range of given machine power')
else disp('The calculated power is out of range of given machine power')
end
disp('----------------------------------------------------------------------------------')
%**************************************************************************
%���� �
disp('Clearence')
disp('***************')
%case b: 12H7/k6
d_min=d2+2*min(y);
d_max=d2+2*max(y);

if (d_max>12+0.018 || d_max<12+0.007)||(d_min>12+0.018 || d_min<12+0.007)
disp('Shaft does not fit to hole in case of 12H7/m6 fit')
else
    disp('Shaft does fit to hole in case of 12H7/m6 fit')
end
disp('----------------------------------------------------------------------------------')
%**************************************************************************
%���� �
disp('Surface roughness')
disp('**************************')
disp('Ra calculation, all units in [mm] :')
Ra_req=0.8e-3%Ra requiered
Rtool=input('Please enter tool radius:');%?????
Rz=f0^2/(8*Rtool);
Ra=Rz/3.95;
disp('**')
if Ra>Ra_req
    disp('=> Ra value is too high ')
    Ra
else disp('=> Ra level is good')
Ra
end

disp('----------------------------------------------------------------------------------')
%*************************************************************************
%���� �
p_machine=15;
a_max=(p_machine*eff)/(Ks*v*f0)*60000;
display(['The max cut depth a is:',num2str(a_max),'[mm]'])

n=1000*v/(pi*d1);
t=L/(f0*n);
display(['The processing time is:',num2str(t),'[min]'])



end