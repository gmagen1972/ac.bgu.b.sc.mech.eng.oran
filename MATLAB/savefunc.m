function savefunc(fname,fun,gr)
%SAVEFUNC savefunc
%savefunc recievs 3 strings, and presents a graph in a random area.
%fname is the file name, fun is the function of x, and gr is a flag for
%grid on/off.
%savefunc saves the workspace off the current function.
a=rand(1).*100;
b=rand(1).*100;
if a>b
    tmp=a;
    a=b;
    b=tmp;
end
ezplot(fun,linspace(a,b,1000))
if nargin==2
    grid on
end
if nargin==3
    if strcmp(gr,'on')
        grid on
    end
end
title(['The function range is between:' ,num2str(a),'<=x<=',num2str(b)])
ylabel(fun)
save (fname)