function targil3
close all
clc
clear all

u=[0:0.01:1];

n=input('Enter size of polynom, n= ');%polynom degree
v=ginput(n+1);%control points

temp=zeros(length(u),2);
    for i=0:n
p=(factorial(n)/(factorial(i)*factorial(n-i))).*(1-u').^(n-i).*u'.^i*v(i+1,:);
temp=temp+p;
plot(v(i+1,1),v(i+1,2),'r *')
hold on
end

plot(temp(:,1),temp(:,2))
grid on
hold on
xlabel('X'),ylabel('Y')
title('Bezier Polynoms')



end