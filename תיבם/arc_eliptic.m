function arc
%eliptic arc

clc
clear all

a=input('Enter  first radius:');
b=input('Enter  second radius:');
teta1=input('Enter initial angle in radian:');
teta2=input('Enter final angle in radian:');
h=input('Enter x cordinate for center of elipse:');
k=input('Enter y cordinate for center of elipse:');

teta=[teta1:0.01:teta2];
 x=a*cos(teta)+h ;y=b*sin(teta)+k;
plot(x,y)
axis equal
grid on
title('Eliptic arc')
xlabel('X');ylabel('Y')

end