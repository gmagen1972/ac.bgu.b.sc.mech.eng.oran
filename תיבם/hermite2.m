function hermite2
clc
clear all
close all

%pc-hermite
u=[0:0.01:1];
f1=1-3*u.^2+2*u.^3;
f2=3*u.^2-2*u.^3;
f3=u-2*u.^2+u.^3;
f4=-u.^2+u.^3;
F=[f1' f2' f3' f4'];
%**************************************************
display('initial point input (p0):')
display(' ----------------------------------------')
p0(1)=input('enter X value: ');
p0(2)=input('enter Y value: ');
display('enter 3 values for k0 : ')
k0_a=input('');
k0_b=input('');
k0_c=input('');
teta0=input('enter initial angel in deg: ');

alfa0=tan(teta0*pi/180);
t0x=1/(sqrt(1+alfa0^2));
t0y=alfa0/(sqrt(1+alfa0^2));
t0=[t0x t0y];
%*************************************************
display(' ')
display('final point input (p1):')
display(' -------------------------------------')
p1(1)=input('enter X value: ');
p1(2)=input('enter Y value: ');
display('enter 3 values for k1 : ');
k1_a=input('');
k1_b=input('');
k1_c=input('');
teta1=input('enter final angel in deg: ');

alfa1=tan(teta1*pi/180);
t1x=1/(sqrt(1+alfa1^2));
t1y=alfa1/(sqrt(1+alfa1^2));
t1=[t1x t1y];
%%*****************************************************

B1=[p0' p1' (k0_a*t0)' (k1_a*t1)'];
B2=[p0' p1' (k0_b*t0)' (k1_b*t1)'];
B3=[p0' p1' (k0_c*t0)' (k1_c*t1)'];
p1=F*B1';
p2=F*B2';
p3=F*B3';

plot(p1(:,1)',p1(:,2)',p2(:,1)',p2(:,2)','r',p3(:,1)',p3(:,2)','g'), hold
axis equal
grid
xlabel('x'), ylabel('y')
legend(['k0=',num2str(k0_a),', k1=',num2str(k1_a)],['k0=',num2str(k0_b),', k1=',num2str(k1_b)],['k0=',num2str(k0_c),', k1=',num2str(k1_c)])
%%%%%%%%%%
%�����
%for the second k:
u1=input('enter u1 value: ');
u2=input('enter u2 value: ');
part1=round(u1*length(p2));
part2=round(u2*length(p2));

plot(p2(part1,1),p2(part1,2),'k o')
plot(p2(part2,1),p2(part2,2),'c o')
end