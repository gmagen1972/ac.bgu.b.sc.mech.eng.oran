function pc_hermite
clc
clear all
close all


u=[0:0.01:1];
r=1;
x=r*cos(pi*u);
y=r*sin(pi*u);
px=[r ;-r ;0 ; 0 ];%hermite points
py=[0; 0; pi*r; -pi*r];

f1=1-3*u.^2+2*u.^3;
f2=3*u.^2-2*u.^3;
f3=u-2*u.^2+u.^3;
f4=-u.^2+u.^3;
p=[f1' f2' f3' f4']*[px py];

plot(x,y,p(:,1)',p(:,2)','r')

    maximum_length=max(((y'-p(:,2)).^2+(x'-p(:,1)).^2).^0.5)
   average_length= sum(((y'-p(:,2)).^2+(x'-p(:,1)).^2).^0.5)/length(u)
    
axis equal
grid
xlabel('x'), ylabel('y'), legend('original','pc-hermite')





















end
