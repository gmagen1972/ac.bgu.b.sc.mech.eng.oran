function bezier1
close all
clc
clear all

u=[0:0.01:1];
%����� ����� �����
n=input('Enter size of polynom, n= ');%polynom degree
v=ginput(n+1);%control points

temp=zeros(length(u),2);
 
for i=0:n
p=(factorial(n)/(factorial(i)*factorial(n-i))).*(1-u').^(n-i).*u'.^i*v(i+1,:);
temp=temp+p;
plot(v(i+1,1),v(i+1,2),'r *')
hold on
legend('control points')
end
hold on
plot(temp(:,1),temp(:,2))

products=length(u)*2*(n+1);
display(['Binomial Coefficient- Number of products is: ',num2str(products)])

grid on
xlabel('X'),ylabel('Y')
title('Bezier Polynoms')

        
%de casteljau
 counter=0;       
   for k=1:101
     Vx=v(:,1);
     Vy=v(:,2);
       
     for i=1:n
         
         for j=1:(n-1)-i+2
                Vx(j)=Vx(j)*(1-u(k))+Vx(j+1)*u(k);
                Vy(j)=Vy(j)*(1-u(k))+Vy(j+1)*u(k);
                counter=counter+4;
         end

     end
        xx(k)=Vx(1);
        yy(k)=Vy(1);
   end
   
display(['De Casteljau- Number of products is: ',num2str(counter)])

k = convhull(v(:,1),v(:,2));
plot(xx, yy,'og', v(k,1),v(k,2),'k')



end