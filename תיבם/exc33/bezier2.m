function bezier2

%H.W3
%2
q=1;
while q==1
    close all;
n=input('choose the polynom');
%i=(0:1:n);
u=(0:0.01:1);
i=0;
[vx vy]=ginput(n+1);
V=[vx vy];length(V)
size(V)
plot(vx, vy,'*')
hold on;
j=1;
pu=0;
while i<=n
    B=(factorial(n)/(factorial(n-i)*factorial(i)));
    u1=(1-u).^(n-i);
    u2=(u.^i);
    u3=u1.*u2;
    pu=pu+B*[V(j,1); V(j,2)]*u3;
    i=i+1;
    j=j+1;
end
size(pu)
plot(pu(1,:), pu(2,:))

hold on;

wx(1)=vx(1);
wy(1)=vy(1);
for i=1:length(V)
    j=i+1;
    if i==length(V)
        wx(j)=vx(i);
        wy(j)=vy(i);
    else
        wx(j)=(j/(length(V)+1))*vx(i)+(1-j/(length(V)+1))*vx(j);
        wy(j)=(j/(length(V)+1))*vy(i)+(1-j/(length(V)+1))*vy(j);
    end
end%for%
    W=[wx' wy'];size(W)
plot(  wx,    wy,'^')
hold on;

%%%%high Bez%%%%

i=0;
j=1;
n=n+1;
pu_2=0;
while i<=n
    B=(factorial(n)/(factorial(n-i)*factorial(i)));
    u1=(1-u).^(n-i);
    u2=(u.^i);
    u3=u1.*u2;
    pu_2=pu_2+B*[W(j,1); W(j,2)]*u3;
    i=i+1;
    j=j+1;
end

plot(pu_2(1,:), pu_2(2,:),'r')
hold on;

%%move control points%%

move=input('enter the control point you would like to move      ');
[V(move,1) V(move,2)]=ginput(1);
plot(V(move,1) ,V(move,2),'o')
hold on;

i=0;
j=1;
n=n-1;
pu_move=0;
while i<=n
    B=(factorial(n)/(factorial(n-i)*factorial(i)));
    u1=(1-u).^(n-i);
    u2=(u.^i);
    u3=u1.*u2;
    pu_move=pu_move+B*[V(j,1); V(j,2)]*u3;
    i=i+1;
    j=j+1;
end

plot(pu_move(1,:), pu_move(2,:),'g')
grid on
 xlabel('X');
 ylabel('Y');
 legend('Original Control points','Original Bezier','Higher Power ','Higher Power Bezier','new  moved Control point','New Bezier','Location','Best')
  title(['Bezier Curves,moving conrol points and higher power']);

q=input('if you want to leave press 0 else press 1     ');
end%while%

end%function%
