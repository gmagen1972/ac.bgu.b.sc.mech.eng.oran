function bezier1

%H.W3
%1
n=input('choose the polynom');
%i=(0:1:n);
u=(0:0.01:1);
i=0;
[vx vy]=ginput(n+1);
V=[vx vy];
plot(vx, vy,'*')

hold on;
X=pascal(n);
pu=0;
j=1;

        %%%Binom&&&
while i<=n
    B=(factorial(n)/(factorial(n-i)*factorial(i)));
    u1=(1-u).^(n-i);
    u2=(u.^i);
    u3=u1.*u2;
    pu=pu+B*[V(j,1); V(j,2)]*u3;
    i=i+1;
    j=j+1;
end
size(pu)
plot(pu(1,:), pu(2,:))

hold on;
        
        %%%%de cast...&&&
        
   for k=1:101
     Vx=vx;
     Vy=vy;
        for i=1:n
            for j=1:(n-1)-i+2
                Vx(j)=Vx(j)*(1-u(k))+Vx(j+1)*u(k);
                Vy(j)=Vy(j)*(1-u(k))+Vy(j+1)*u(k);
            end
        end
        xx(k)=Vx(1);
        yy(k)=Vy(1);
   end

hold on;
k = convhull(vx,vy);
plot(xx, yy,'^', vx(k),vy(k),'g')
 grid on
 xlabel('X');
 ylabel('Y');
 legend('control Points','Binomial Coef','DeCasteljeau','Convex hull','Location','Best')
  title(['Bezier Curves,number of conrol points: ',num2str(n)]);





end