function spline4

n=input('Enter number of points for the spline: ');
[x y]=ginput(n);
u=[0:0.01:1];

if (n==2)
    B=[2 1;1 2];
else
    B(1,1:2)=[1 2];
    for i=2:n-1
        B(i,i-1:i+1)=[1 4 1];
        xx(i,1)=3*(x(i+1)-x(i-1));
        yy(i,1)=3*(y(i+1)-y(i-1));
        i=i+1;
    end
    B(n,n-1:n)=[1 2];
end
xx(1,1)=3*(x(2)-x(1));
xx(n,1)=3*(x(n)-x(n-1));
yy(1,1)=3*(y(2)-y(1));
yy(n,1)=3*(y(n)-y(n-1));
dx=B\xx;
dy=B\yy;

F1=(1-3.*u.^2+2.*u.^3);
F2=(3.*u.^2-2.*u.^3);
F3=(u-2.*u.^2+u.^3);
F4=(-u.^2+u.^3);
    
i=1;
while(i<n);
    XX(1:2)=x(i:i+1);
    XX(3:4)=dx(i:i+1);
    YY(1:2)=y(i:i+1);
    YY(3:4)=dy(i:i+1);
    
    XX=XX(1).*F1+XX(2).*F2+XX(3).*F3+XX(4).*F4;
    YY=YY(1).*F1+YY(2).*F2+YY(3).*F3+YY(4).*F4;
 
    del_XX=diff(XX)./diff(u);
    del_YY=diff(YY)./diff(u);
    
    d_XX=diff(del_XX)./diff(u(1:100));
    d_YY=diff(del_YY)./diff(u(1:100));
    ro=((d_YY.*del_XX(1:99))-(del_YY(1:99).*d_XX))./((del_XX(1:99)).^2+(del_YY(1:99)).^2).^(3/2);
        
        plot(u(1:99)+i-1,ro);   
        grid on
         title('CURVATURE')
        i=i+1;
end

end