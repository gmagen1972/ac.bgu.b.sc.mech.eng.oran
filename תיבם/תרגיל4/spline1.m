function spline1

n=input('Enter number of points for the spline: ');
[x y]=ginput(n);
plot(x,y,'o')
hold on
u=[0:0.01:1];

if (n==2)
    B=[2 1;1 2];
else
    B(1,1:2)=[1 2];
    for i=2:n-1
        B(i,i-1:i+1)=[1 4 1];
        xx(i,1)=3*(x(i+1)-x(i-1));
        yy(i,1)=3*(y(i+1)-y(i-1));
        i=i+1;
    end
    B(n,n-1:n)=[1 2];
 end

xx(1,1)=3*(x(2)-x(1));
xx(n,1)=3*(x(n)-x(n-1));
yy(1,1)=3*(y(2)-y(1));
yy(n,1)=3*(y(n)-y(n-1));
dx=B\xx;
dy=B\yy;

F1=(1-3.*u.^2+2.*u.^3);
F2=(3.*u.^2-2.*u.^3);
F3=(u-2.*u.^2+u.^3);
F4=(-u.^2+u.^3);
    
i=1;
while(i<n);
    XX(1:2)=x(i:i+1);
    XX(3:4)=dx(i:i+1);
    YY(1:2)=y(i:i+1);
    YY(3:4)=dy(i:i+1);
    
    XX=XX(1).*F1+XX(2).*F2+XX(3).*F3+XX(4).*F4;
    YY=YY(1).*F1+YY(2).*F2+YY(3).*F3+YY(4).*F4;
     
 plot(XX,YY,'r')
grid on
title('Cubic Spline'), xlabel('X'), ylabel('Y')
        i=i+1;
end