clc
clear all
close all

% create a cubic spline
n=input('Please enter number of points you want to enter for the cubic spline: ');
disp('Please enter the points you wants the cubic spline will pass through them  ')
[px py]=ginput(n);

uu=input('Please enter number of points for rezolution: ');
u=linspace(0,1,uu);

m=n-1; 


for i=1 : m+1
    for  j=1 :m+1
        A(i,j)=0;
    end
end

for i=1 : m+1
   
if i==1       
    A(1,1)=2;
     A(1,2)=1;
end
if i>1  && i<m+1
        A(i,i-1)=1;
         A(i,i)=4;
         A(i,i+1)=1; 
end
if i==m+1       
     A(m+1,m)=1;
     A(m+1,m+1)=2;
    end 
 
end

for i=1 :m+1
    if i==1          
         hx(i)=3*(px(2)-px(1));
        hy(i)=3*(py(2)-py(1));
    end
    if i>1  && i<m+1
         hx(i)=3*(px(i+1)-px(i-1));
        hy(i)=3*(py(i+1)-py(i-1));
    end
    if i==m+1       
        hx(i)=3*(px(m+1)-px(m));
        hy(i)=3*(py(m+1)-py(m));
    end
end
 inv1=inv(A);
  
Dx=inv1*hx';
Dy=inv1*hy';


for i=1 :m
    
DD=Dx(i) ;       
DD1=Dx(i+1);
ax(i)=px(i);  
bx(i)=DD;
cx(i)=3*(px(i+1)-px(i))-2*DD-DD1;
dx(i)=2*(px(i)-px(i+1))+DD+DD1;

%Yx(i)=ax(i)+bx(i).*u+cx(i).*u.^2+dx(i).*u.^3;
%dYx(i)=bx(i)+2*cx(i).*u+3*dx(i).*u.^2;


 DD=Dy(i);
DD1=Dy(i+1);
ay(i)=py(i);  
by(i)=Dy(i);
cy(i)=3*(py(i+1)-py(i))-2*DD-DD1;
dy(i)=2*(py(i)-py(i+1))+DD+DD1;

%Yy(i)=ay(i)+by(i).*u+cy(i).*u.^2+dy(i).*u.^3;
%dYy(i)=by(i)+2*cy(i).*u+3*dy(i).*u.^2;

 

end
for i=1 : m;
 p0x=px(i);            
 p0y=py(i);
 p1x=px(i+1);
 p1y=py(i+1);
 dp0x=Dx(i);
 dp0y=Dy(i);
 dp1x=Dx(i+1);
 dp1y=Dy(i+1);

 Bx=[ p0x p1x dp0x dp1x ];
  By=[p0y p1y dp0y dp1y];


  F1=1-3.*(u.^2)+2.*(u.^3);
  F2=3.*(u.^2)-2.*(u.^3);
  F3=u-2.*(u.^2)+u.^3;
  F4=-(u.^2)+u.^3;
  
 F=[F1; F2; F3; F4]';
 Pux=F*Bx';
 Puy=F*By';
  
 plot(Pux,Puy);
 hold on;
end
 grid on
 xlabel('X')
 ylabel('Y')
 title('Cubic - Spline')
 
 