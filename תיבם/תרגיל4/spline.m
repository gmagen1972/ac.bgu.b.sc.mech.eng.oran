function q=targil4A()
figure(1)
plot(0,0,'.',100,100,'.')
[yx,yy]=ginput();
plot(yx,yy,'*')
n=size(yx);
n=n(1,1);
U=100000;
u=linspace(0,1,U);
if (n==2)
    B=[2 1;1 2];
else
    B=zeros(n);
    i=2;
    B(1,1:2)=[1 2];
    while(i<n)
        B(i,i-1:i+1)=[1 4 1];
        Yx(i,1)=3*(yx(i+1)-yx(i-1));
        Yy(i,1)=3*(yy(i+1)-yy(i-1));
        i=i+1;
    end
    B(n,n-1:n)=[1 2];
    
end
Yx(1,1)=3*(yx(2)-yx(1));
Yy(1,1)=3*(yy(2)-yy(1));
Yx(n,1)=3*(yx(n)-yx(n-1));
Yy(n,1)=3*(yy(n)-yy(n-1));
Dx=B\Yx;
Dy=B\Yy;
hold on
q=0;
pol(q,Dx,Dy,u,U,n,yx,yy)
hold off
q=input('press 1 if you want the qurvature graph');
if (q==1)
    figure(2)
    hold on
    pol(q,Dx,Dy,u,U,n,yx,yy)
    hold off
end
end
function r=pol(q,Dx,Dy,u,U,n,yx,yy)
i=1;
while(i<n);
    px(1:2)=yx(i:i+1);
    px(3:4)=Dx(i:i+1);
    py(1:2)=yy(i:i+1);
    py(3:4)=Dy(i:i+1);
    F1=(1-3.*u.^2+2.*u.^3);
    F2=(3.*u.^2-2.*u.^3);
    F3=(u-2.*u.^2+u.^3);
    F4=(-u.^2+u.^3);
    Px=px(1).*F1+px(2).*F2+px(3).*F3+px(4).*F4;
    Py=py(1).*F1+py(2).*F2+py(3).*F3+py(4).*F4;
    if (q==0)
        plot(Px,Py,'r');
    end
    if(q==1)
        dpx=diff(Px)./diff(u);
        dpy=diff(Py)./diff(u);
        ddpx=diff(dpx)./diff(u(1:U-1));
        ddpy=diff(dpy)./diff(u(1:U-1));
        ro=((ddpy.*dpx(1:U-2))-(dpy(1:U-2).*ddpx))./((dpx(1:U-2)).^2+(dpy(1:U-2)).^2).^(3/2);
        plot(i-1+u(1:U-2),ro);
    end
    i=i+1;
end

end