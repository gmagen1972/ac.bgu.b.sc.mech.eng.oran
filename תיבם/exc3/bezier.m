function  bezier()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
selection = 1;

%***main menu***%
clc
close all
while selection ~= 0

disp(' ');
disp('1. Bezier Curve. ');
disp('2. Bezier power raising ');
disp('3. Bezier Subdivision ');
disp('4. Bezier Interpolation');
disp('0. EXIT ');
disp(' ');
selection = input('Please enter your selection: ');
 
res = 100;

u = linspace(0,1,res);

    switch selection

        case 1 %*** - circle - ***%
            close all
          controlNum = input('Enter number of control points: ');  
          
          [x y] = ginput(controlNum);
          controlNum = controlNum-1;
          Bez_x = 0;
          Bez_y = 0;
          
          %******* Binom ********%
          for i=0:controlNum
              Bez_x = Bez_x+x(i+1)*nchoosek(controlNum,i)*(1-u).^(controlNum-i).*u.^i;
              Bez_y = Bez_y+y(i+1)*nchoosek(controlNum,i)*(1-u).^(controlNum-i).*u.^i;
          end
          
          %*******  DeCasteljue **********%
          for k=1:res
                x_Castel=x;
                y_Castel=y;
                for i=1:controlNum
                    for j=1:(controlNum-1)-i+2
                        x_Castel(j)=x_Castel(j)*(1-u(k))+x_Castel(j+1)*u(k);
                        y_Castel(j)=y_Castel(j)*(1-u(k))+y_Castel(j+1)*u(k);
                    end
                end
                xx(k)=x_Castel(1);
                yy(k)=y_Castel(1);
          end  

          k = convhull(x,y);   
          plot(x,y,'*',Bez_x,Bez_y,xx,yy,'^',x(k),y(k));
          grid on
          legend('control','Binomial','DeCasteljeau','Convex hull','Location','Best')
          title(['Bezier Curves,number of conrol points: ',num2str(controlNum)]);
          k = convhull(x,y);
        
        case 2 %*** - Higher Power - ***%
        close all
          controlNum = input('Enter number of control points: ');  
          d = controlNum;
          [x y] = ginput(controlNum);
          controlNum=controlNum-1;
          
          power = input('How many powers do you wish to raise: ');
          [high_x,high_y] = raisePower(x,y,power);
          [Bez_x Bez_y] = calcBezier(x,y,50); 
          [Bez_x_h,Bez_y_h] = calcBezier(high_x,high_y,100);
       
         plot(x,y,'*--',high_x,high_y,'d--',Bez_x_h,Bez_y_h,'b',Bez_x,Bez_y,'^')
         grid on
         legend('control','New Control','high power','original power','Location','Best')
         hold on;
         
        mover = 1;
        
         while mover ~= 0
             x_changed = x;
             y_changed = y;
             disp('Enter 0 if you do not wish to change');
         mover = input(['Enter number of original control point you would like to move (1 - ',num2str(d),'): ']);
           if mover == 0 break; end
         figure(1)
         plot(x(mover),y(mover),'dr','LineWidth',2);
         disp('the control point you have chosen is marked with a red diamond on the graph.');
         disp('Please click on the graph to set the new location of the control point');
         [x_changed(mover) y_changed(mover)] = ginput(1);
         [newBez_x newBez_y] = calcBezier(x_changed,y_changed,100);
         figure(2)
         plot(Bez_x,Bez_y,x_changed,y_changed,'*--',newBez_x,newBez_y);
         legend('original','new control','changed');
         grid on
         
         end
         
        case 3 %*** - Helix - ***%
close all
%calculating the curve with  De Casteljau
num = input('Enter number of control points: ');  
          
[x y] = ginput(num);
u_cut=input('enter the parameter value in which you want to divide the curves : ');

    x_Castel=x;
    y_Castel=y;
    for i=1:num-1
        for j=1:(num-1)-i+1
            x_Castel(j)=x_Castel(j)*(1-u_cut)+x_Castel(j+1)*u_cut;
            y_Castel(j)=y_Castel(j)*(1-u_cut)+y_Castel(j+1)*u_cut;
        end
    x_1(i)=x_Castel(1);
    y_1(i)=y_Castel(1);
    x_2(i)=x_Castel(num+1-i);
    y_2(i)=y_Castel(num+1-i);

    end
    
x_1=[x(1),x_1];
y_1=[y(1),y_1];
x_2=[x_2,x_Castel(1)];
y_2=[y_2,y_Castel(1)];

%calculating the curve with  binomial  const.
x_1_binom=zeros(1,res);
y_1_binom=zeros(1,res);
x_2_binom=zeros(1,res);
y_2_binom=zeros(1,res);

for i=1:num
    ncr=factorial(num-1)/(factorial(i-1)*factorial(num-1-(i-1)));
    x_1_binom=x_1_binom+ncr.*(1-u).^(num-i).*u.^(i-1).*x_1(i);
    y_1_binom=y_1_binom+ncr.*(1-u).^(num-i).*u.^(i-1).*y_1(i);
    x_2_binom=x_2_binom+ncr.*(1-u).^(num-i).*u.^(i-1).*x_2(i);
    y_2_binom=y_2_binom+ncr.*(1-u).^(num-i).*u.^(i-1).*y_2(i);

end

plot(x_1_binom,y_1_binom,'k',x_2_binom,y_2_binom,'r')
grid on
title('Bezier SubDivision');
legend('1st curve','2nd curve');
            
        case 4 %*** -  - ***%
          
            close all
          controlNum = input('Enter number of interpolation points: ');  
          u = linspace(0,1,controlNum);
          [x y] = ginput(controlNum);
          controlNum= controlNum-1;
          
         %**** UNIFORM distribuatioin ****%
        
        for i=1:length(u)  
            for j=0:controlNum
                  uni_mat(i,j+1) = nchoosek(controlNum,j)*(1-u(i))^(controlNum-j)*u(i)^j;
                
            end
        end
        
        %****  Length Distribuation ***%
        dx = diff(x);
        dy = diff(y);
        dist = sqrt(dx.^2+dy.^2);
        v = [0;cumsum(dist)/sum(dist)];
         for i=1:length(v)  
            for j=0:controlNum
                  dist_mat(i,j+1) = nchoosek(controlNum,j)*(1-v(i))^(controlNum-j)*v(i)^j;
                
            end
         end
      x_uni = inv(uni_mat)*x;
      y_uni = inv(uni_mat)*y;
      x_dist = inv(dist_mat)*x;
      y_dist = inv(dist_mat)*y;
      
      [x_bez_uni y_bez_uni] = calcBezier(x_uni,y_uni,100);
      [x_bez_dist y_bez_dist] = calcBezier(x_dist,y_dist,100);
      plot(x_bez_uni, y_bez_uni,x_bez_dist, y_bez_dist);
      hold on
       plot(x,y,'dr','LineWidth',2)
      legend('Chosen points','Unified dis.','length dis.','Location','best');      
      grid on
        case 0
            disp(' ')
            disp('Hope you enjoyed! GOODBYE!')
            return
            
    end  %switch ends%
    
end %while
end


function [xx yy] = raisePower(x,y,power)
        xx = x;
        yy = y;
        if power ~= 0
       
        controlNum = length(x)-1;  
        d = controlNum+1;
        high_x = x(1);
        high_y = y(1);
        
          for i = 1:controlNum
              j = i+1;
              high_x(j)=(i/(d))*x(j-1)+(1-i/(d))*x(j);
              high_y(j)=(i/(d))*y(j-1)+(1-i/(d))*y(j);
          end
          
           xx=[high_x,x(length(x))];
           yy=[high_y,y(length(y))];
           
       [xx yy]= raisePower(xx,yy,power-1);
        end
        
     return
     
     end

function [x_bez, y_bez] = calcBezier( x , y , res)
    Bez_x=0;
    Bez_y=0;
    controlNum = length(x)-1;
    u = linspace(0,1,res);
        for i=0:controlNum
              Bez_x = Bez_x+x(i+1)*nchoosek(controlNum,i)*(1-u).^(controlNum-i).*u.^i;
              Bez_y = Bez_y+y(i+1)*nchoosek(controlNum,i)*(1-u).^(controlNum-i).*u.^i;          
        end
          
    x_bez= Bez_x;
    y_bez = Bez_y;
    
end

