function helix

clc
clear all

r=input('Enter   radius:');
a=input('Enter x cordinate for center helix:');
b=input('Enter y cordinate for center helix:');
n=input('Enter num of pitches :');
h=input('Enter size of pitch :');
u=[0 :0.01 :2*pi];
 x=r*cos(n*u)+a; y=r*sin(n*u)+b;
 z=h/(2*pi)*u;
plot3(x,y,z)
axis equal
grid on
title('helix')
xlabel('X');ylabel('Y') ;zlabel('Z');

end