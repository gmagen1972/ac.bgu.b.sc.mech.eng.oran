function subdivision
close all
clc
clear all

u = linspace(0,1,100);
n=input('Enter number of control points: ');         
[x y] = ginput(n);
s=input('enter the parameter value for the division : ');
%De Casteljau
    x_C=x;
    y_C=y;
    for i=1:n-1
        for j=1:n-i
            x_C(j)=x_C(j)*(1-s)+x_C(j+1)*s;
            y_C(j)=y_C(j)*(1-s)+y_C(j+1)*s;
        end
    x_1(i)=x_C(1);
    y_1(i)=y_C(1);
    x_2(i)=x_C(n+1-i);
    y_2(i)=y_C(n+1-i);
   end
    
x_1=[x(1),x_1];
y_1=[y(1),y_1];
x_2=[x_2,x_C(1)];
y_2=[y_2,y_C(1)];

%Binomial Co':
x1_b=zeros(1,100);
y1_b=zeros(1,100);
x2_b=zeros(1,100);
y2_b=zeros(1,100);
for i=1:n
  m=factorial(n-1)/(factorial(i-1)*factorial(n-i));
    x1_b=x1_b+m.*(1-u).^(n-i).*u.^(i-1).*x_1(i);
    y1_b=y1_b+m.*(1-u).^(n-i).*u.^(i-1).*y_1(i);
    x2_b=x2_b+m.*(1-u).^(n-i).*u.^(i-1).*x_2(i);
    y2_b=y2_b+m.*(1-u).^(n-i).*u.^(i-1).*y_2(i);
end

plot(x1_b,y1_b,x2_b,y2_b,'r')
title('Bezier Sub-Division')
legend('1st curve','2nd curve')
grid on
end