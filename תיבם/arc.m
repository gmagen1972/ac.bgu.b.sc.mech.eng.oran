function arc
%round arc

clc
clear all

r=input('Enter radius:');
teta1=input('Enter initial angle in radian:');
teta2=input('Enter final angle in radian:');
a=input('Enter x cordinate for center of circle:');
b=input('Enter y cordinate for center of circle:');

teta=[teta1:0.01:teta2];
 x=r*cos(teta)+a ;y=r*sin(teta)+b;
plot(x,y)
axis equal
grid on
title('Circular arc')
xlabel('X');ylabel('Y')

end