clc
clear all
close all
syms u

n=input(' enter number of point for Bezier curve  :')  ;           %number of point
[vx,vy]=ginput(n) ;                                                                                               %control point
n=n-1;
plot(vx,vy,'r:+')
hold on
px=0;
py=0;
for i=1:1:n+1
    s=i-1;
    r=n;
    
    B(i)=(factorial(r)/    (factorial(r-s)*factorial(s))     ) * u^s * (1-u)^(r-s);  %basic function
    px=px+vx(i)*B(i);
    py=py+vy(i)*B(i);
end
u=0:0.01:1;

px=subs(px);
py=subs(py);

plot(px,py)
title('Bezier curve')
grid













