function inter
close all
clc
clear all
axis([0 10 0 10]) 
 n = input('Enter number of interpolation points: ');  
          u = linspace(0,1,n);
       
          [x y] = ginput(n);
          
%**** uniform distribuatioin ****%
        for i=1:length(u)  
            for j=0:n-1
                  M(i,j+1) = factorial(n-1)/(factorial(j)*factorial(n-1-j))*(1-u(i))^(n-1-j)*u(i)^j;
             end
        end
    x_u = inv(M)*x;
    y_u = inv(M)*y; 
    
        %****  Length Distribuation ***%
        dx = diff(x);
        dy = diff(y);
        d = sqrt(dx.^2+dy.^2);
        v = [0;cumsum(d)/sum(d)];
         for i=1:length(v)  
            for j=0:n-1
                  dist(i,j+1) = factorial(n-1)/(factorial(j)*factorial(n-1-j))*(1-v(i))^(n-1-j)*v(i)^j;
            end
         end
         
      x_d = inv(dist)*x;
      y_d = inv(dist)*y;
   %bezier calculations :  
    X=0;Y=0;
    XX=0;YY=0;
     u = linspace(0,1,100);
  
     %uniform distribuatioin:
     n = length(x_u)-1;
        for i=0:n
              X = X+x_u(i+1)*(factorial(n)/(factorial(i)*factorial(n-i)))*(1-u).^(n-i).*u.^i;
              Y = Y+y_u(i+1)*(factorial(n)/(factorial(i)*factorial(n-i)))*(1-u).^(n-i).*u.^i;          
        end
%Length Distribuation:
        n = length(x_d)-1;
        for i=0:n
              XX =XX+x_d(i+1)*(factorial(n)/(factorial(i)*factorial(n-i)))*(1-u).^(n-i).*u.^i;
              YY = YY+y_d(i+1)*(factorial(n)/(factorial(i)*factorial(n-i)))*(1-u).^(n-i).*u.^i;          
        end
        
      plot(X,Y,XX,YY,'m');
      hold on
       plot(x,y,'or')
      legend('uniform distribuatioin','Length Distribuation','user points');      
      grid on
 
end